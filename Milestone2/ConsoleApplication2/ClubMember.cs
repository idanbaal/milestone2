﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BL_backend
{
    [Serializable]
    public class ClubMember : Person
    {
        public List<string> transactionID;
        public int memberID;
        public DateTime dateOfBirth;
        public User user;
        private int creditCardThreeDigits;
        long creditCardNumber;

        public int CreditCardThreeDigits
        {
            get { return creditCardThreeDigits; }
            set { creditCardThreeDigits = value; }
        }

        public long CreditCardNumber
        {
            get { return creditCardNumber; }
            set { creditCardNumber = value; }
        }
        private DateTime creditCardExpiration;

        public DateTime CreditCardExpiration
        {
            get { return creditCardExpiration; }
            set { creditCardExpiration = value; }
        }



        public ClubMember(int memberID, DateTime dateOfBirth, int teudatZeut, string firstName, string lastName, string gender, User user, DateTime creditCardExpiration, long creditCardNumber, int creditCardThreeDigits)
            : base(teudatZeut, firstName, lastName, gender)
        {
            transactionID = new List<string>();
            this.memberID = memberID;
            this.dateOfBirth = dateOfBirth;
            this.user = user;
            this.creditCardExpiration = creditCardExpiration;
            this.creditCardNumber = creditCardNumber;
            this.creditCardThreeDigits = creditCardThreeDigits;
        }

        public ClubMember(ClubMember c)
            : base(c.TeudatZeut, c.FirstName, c.LastName, c.Gender)
        {
            transactionID = c.transactionID;
            this.memberID = c.memberID;
            this.dateOfBirth = c.dateOfBirth;
            this.user = c.user;
        }

        public ClubMember()
            : base(0, "", "", "")
        {
            memberID = 0;
            dateOfBirth = new DateTime(2015, 1, 1);


        }

        public int MemberID
        {
            get { return memberID; }
            set { memberID = value; }
        }


        public DateTime DateOfBirth
        {
            get { return dateOfBirth; }
            set { dateOfBirth = value; }
        }



        public int getMemberId()
        {
            return memberID;
        }
        public void setMemberId(int MemberId)
        {
            this.memberID = MemberId;
        }

        public DateTime getDateOfBirth()
        {
            return dateOfBirth;
        }
        public void setDateOfBirth(DateTime dateOfBirth)
        {
            this.dateOfBirth = dateOfBirth;
        }

        // public transactionID(int id)
        //{
        //    public int id;

        //      public transactionID(int id)
        //    {
        //       this.id = Transaction.





      


    }

}