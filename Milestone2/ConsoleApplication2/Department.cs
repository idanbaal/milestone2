﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_backend
{
    [Serializable]
    public class Department
    {
        private string name;
        private int departmentID;


        public Department(string name, int departmentID)
        {
            this.name = name;
            this.departmentID = departmentID;
        }

        public Department()
        {
            name = "";
            departmentID = 0;
        }

        public int DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }



        //public string getName()
        //{
        //    return name;
        //}
        //public void setName(string name)
        //{
        //    this.name = name;
        //}
        //public int getID()
        //{
        //    return departmentID;
        //}
        //public void setID(int departmentID)
        //{
        //    this.departmentID = departmentID;
        //}

        public override string ToString()
        {
            return "Department Name : " + Name + "\n" + "Department ID : " + DepartmentID.ToString();
        }

    }
}
