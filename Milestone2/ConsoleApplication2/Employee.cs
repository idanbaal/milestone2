﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_backend
{
    [Serializable]
    public class Employee : Person
    {
        private int departmentID, supervisor;
        private double salary;




        public Employee(double salary, int departmentID, int supervisor, int teudatZeut, string firstName, string lastName, string gender)
            : base(teudatZeut, firstName, lastName, gender)
        {
            this.supervisor = supervisor;
            this.salary = salary;
            this.departmentID = departmentID;
        }

        public Employee()
            : base(0, "", "", "")
        {
            supervisor = 0;
            salary = 0;
            departmentID = 0;


        }

        public Employee(Employee employee)
            : base(employee.TeudatZeut, employee.FirstName, employee.LastName, employee.Gender)
        {
            departmentID = employee.departmentID;
            supervisor = employee.supervisor;
            salary = employee.salary;
        }


        public double Salary
        {
            get { return salary; }
            set { salary = value; }
        }

        public int Supervisor
        {
            get { return supervisor; }
            set { supervisor = value; }
        }

        public int DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }

        //        public int getSup()
        //        {
        //            return supervisor;
        //        }
        //        public void setSup(int supervisor)
        //        {
        //            this.supervisor = supervisor;
        //        }

        //public int getDepID()
        //{
        //    return departmentID;
        //}
        //public void setDepID(int departmentID)
        //{
        //    this.departmentID = departmentID;
        //}


        //public double getSalary()
        //{
        //    return salary;
        //}
        //public void setSalary(double sal)
        //{
        //    salary = sal;
        //}

       

    }
}