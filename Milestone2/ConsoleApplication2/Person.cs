﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_backend
{
    [Serializable]
    public class Person
    {
        public int teudatZeut;
        public string firstName, lastName, gender;

        public Person(int teudatZeut, string firstName, string lastName, string gender)
        {
            this.teudatZeut = teudatZeut;
            this.firstName = firstName;
            this.lastName = lastName;
            this.gender = gender;
        }
        public Person() { }



        public string Gender
        {
            get { return gender; }
            set { gender = value; }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        public int TeudatZeut
        {
            get { return teudatZeut; }
            set { teudatZeut = value; }
        }
     




        public int getID()
        {
            return teudatZeut;
        }

        public void setID(int teudatZeut)
        {
            this.teudatZeut = teudatZeut;
        }


        public string getFirstName()
        {
            return firstName;
        }


        public string getLasttName()
        {
            return lastName;
        }

        public string getGender()
        {
            return gender;
        }



        public void setFirstName(String firstName)
        {
            this.firstName = firstName;
        }


        public void setLastName(String lastName)
        {
            this.lastName = lastName;
        }

        public void getGender(string gender)
        {
            this.gender = gender;
        }

        public void setGender(String gender)
        {
            this.gender = gender;
        }

        //public string toString()
        //{
        //    return "SSN : " + teudatZeut.ToString() + "\n" + "First Name : " + firstName + "Last Name : " + lastName + "\n" + "Gender : " + gender;
        //}

    }
}