﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_backend
{
    [Serializable]
    public enum productTypes
    {
        Electronics, Furniture, Cleaning, None
    } ;
    [Serializable]
    public class Product
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private int InventoryID;
        public double Price;
        private int stockCount;
        private Boolean inStock;
        private int location;

        public int Location
        {
            get { return location; }
            set { location = value; }
        }
        private productTypes type;

        public productTypes Type
        {
            get { return type; }
            set { type = value; }
        }





        public Product(string name, productTypes type, int inventoryID, int location, Boolean inStock, int stockCount, double price)
        {
            this.name = name;
            this.type = type;
            this.InventoryID = inventoryID;
            this.location = location;
            this.inStock = inStock;
            this.stockCount = stockCount;
            this.Price = price;
        }

        public Product()
        {
            name = "NULL";
            type = productTypes.None;
            InventoryID = 0;
            location = 0;
            inStock = true;
            stockCount = 0;
            Price = 0;
        }

        public Product(Product product)
        {
            this.name = product.name;
            this.type = product.type;
            this.InventoryID = product.InventoryID1;
            this.location = product.location;
            this.inStock = product.InStock1;
            this.stockCount = product.stockCount;
            this.Price = product.Price1;
        }



        public string getName()
        {
            return Name;
        }

        public void setName(string name)
        {
            this.Name = name;
        }

        public productTypes getType()
        {
            return Type;
        }
        public void setType(productTypes type)
        {
            this.Type = type;
        }

        public int getLocation()
        {
            return Location;
        }
        public void setLocation(int location)
        {
            this.Location = location;
        }

        public int getID()
        {
            return InventoryID;
        }
        public void setID(int inventoryID)
        {
            this.InventoryID = inventoryID;
        }
      


        public int InventoryID1
        {
            get { return InventoryID; }
            set { InventoryID = value; }
        }


       


        public Boolean InStock1
        {
            get { return inStock; }
            set { inStock = value; }
        }


        public int StockCount
        {
            get { return stockCount; }
            set { stockCount = value; }
        }



        public double Price1
        {
           get { return Price; }
            set { Price = value; }
        }

        //public string getName()
        //{
        //    return Name;
        //}
        //public void setName(string name)
        //{
        //    this.Name = name;
        //}

        //public productTypes getType()
        //{
        //    return Type;
        //}
        //public void setType(productTypes type)
        //{
        //    this.Type = type;
        //}

      //  public int getLocation()
      //  {
       //     return location;
       // }
     //   public void setLocation(int location)
      //  {
       //     this.location = location;
     //   }

        //public int getID()
        //{
        //    return InventoryID1;
        //}
        //public void setID(int inventoryID)
        //{
        //    this.InventoryID1 = inventoryID;
        //}


        //public bool InStock()
        //{
        //    return InStock1;
        //}
        //public void setStock(bool inStock)
        //{
        //    this.InStock1 = inStock;
        //}
        public int getSC()
        {
            return StockCount;
        }
        //public void setSC(int stockCount)
        //{
        //    this.StockCount = stockCount;
        //}
        public double getPrice()
        {
           return Price;
        }
        //public void setPrice(double price)
        //{
        //    this.Price1 = price;
        //}

        //public override string ToString()
        //{
        //    switch (type)
        //    {
        //        case productTypes.Cleaning:
        //            return "Product Name : " + name + "\n" + "Inventory ID : " + InventoryID1.ToString() + "\n" + "Location : " + location.ToString() + "\n" + "Is it in stock? " + InStock1.ToString() + "\n" + " count in stock: " + StockCount.ToString() + "\n" + "Price: " + .ToString() + "\n" + "Type : " + productTypes.Cleaning.ToString();
        //            break;
        //        case productTypes.Electronics:
        //            return "Product Name : " + name + "\n" + "Inventory ID : " + InventoryID1.ToString() + "\n" + "Location : " + location.ToString() + "\n" + "Is it in stock? " + InStock1.ToString() + "\n" + " count in stock: " + StockCount.ToString() + "\n" + "Price: " + Price1.ToString() + "\n" + "Type : " + productTypes.Electronics.ToString();
        //            break;
        //        case productTypes.Furniture:
        //            return "Product Name : " + name + "\n" + "Inventory ID : " + InventoryID1.ToString() + "\n" + "Location : " + location.ToString() + "\n" + "Is it in stock? " + InStock1.ToString() + "\n" + " count in stock: " + StockCount.ToString() + "\n" + "Price: " + Price1.ToString() + "\n" + "Type : " + productTypes.Furniture.ToString();
        //            break;
        //        case productTypes.None:
        //            return "Product Name : " + name + "\n" + "Inventory ID : " + InventoryID1.ToString() + "\n" + "Location : " + location.ToString() + "\n" + "Is it in stock? " + InStock1.ToString() + "\n" + " count in stock: " + StockCount.ToString() + "\n" + "Price: " + Price1.ToString() + "\n" + "Type : " + productTypes.None.ToString();
        //            break;
        //    }
        //    return "Product Name : " + name + "\n" + "Inventory ID : " + InventoryID1.ToString() + "\n" + "Location : " + location.ToString() + "\n" + "Is it in stock? " + InStock1.ToString() + "\n" + " count in stock: " + StockCount.ToString() + "\n" + "Price: " + Price1.ToString() + "\n" + "Type : " + productTypes.None.ToString();

        //}


       
    }
}
