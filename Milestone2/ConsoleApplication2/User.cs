﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_backend
{
    [Serializable]
    public class User
    {
        public enum userType
        {
            Admin, Manager, Worker, Customer, ClubMember
        }
        private string userName;


        private int password;


       
        private userType type;

        public userType Type
        {
            get { return type; }
            set { type = value; }
        }

        public User(string userName, int password, userType type)
        {
            this.userName = userName;
            this.password = password;
            this.type = type;
        }

        public User(string userName, int pass)
        {
            // TODO: Complete member initialization
            this.userName = userName;
            this.password = pass;
        }
        public int Password
        {
            get { return password; }
            set { password = value; }
        }
        public string UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        // override object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            User other = (User)obj;
            if (this.userName.Equals(other.userName) && this.password.Equals(other.password) && this.type.Equals(other.type))
            {
                return true;
            }
            return false;

        }


        //public string UserName
        //{
        //    get;
        //    set;
        //}


    }
}
