﻿using BL_backend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BL
{
    public class product_BL : IBL
    {
        public IDAL itsDAL;
        public product_BL(IDAL dal)// note that the constructor expects an IDAL type (and not a specific implementaion specific)
        {
            itsDAL = dal;
        }



        public bool FindUser(string userName, int password)
        {
            return itsDAL.IsUserExist(new User(userName, password));
        }

        public bool FindProduct(int Id)
        {
            if (itsDAL.FindProductByID(Id) == null)
            {
                return false;
            }
            else return true;
        }

        public Enum FindUserType(string user, int pass)
        {
            return itsDAL.FindUserType(user, pass);
        }

        public List<Transaction> GetAllTransactionListByMonth(int month)
        {
            List<Transaction> Tr = itsDAL.GetTransactiontListByMonth(month);

            return Tr;
        }

        public ClubMember GetClubMemberByUser(string userName)
        {
            if (itsDAL.GetClubMemberByUser(userName) != null)
            {
                return itsDAL.GetClubMemberByUser(userName);
            }
            return null;
        }


        public List<Employee> FindManagerByID(int TZ)
        {

            return itsDAL.FindManagerByID(TZ);
        }

        public Employee getEmp(string userName)
        {
            return itsDAL.getEmp(userName);
        }


        public Product GetBestSeller(int month)
        {
            List<Transaction> transactionList = GetAllTransactionListByMonth(month);
            Dictionary<int, int> productsQuantity = new Dictionary<int, int>();
            foreach (Transaction transaction in transactionList)
            {
                foreach (KeyValuePair<int, BL_backend.Transaction.SoldProducts> soldProduct in transaction.Receipt)
                {
                    int id = soldProduct.Value.ID1;
                    int quantity = soldProduct.Value.Quantity;
                    if (!(productsQuantity.ContainsKey(id)))
                    {
                        productsQuantity.Add(id, 0);
                    }
                    productsQuantity[id] += quantity;
                }
            }
            int max = 0;
            int maxId = -1;
            foreach (KeyValuePair<int, int> quantity in productsQuantity)
            {

                if (quantity.Value > max)
                {
                    max = quantity.Value;
                    maxId = quantity.Key;
                }
            }

            Product product = itsDAL.FindProductByID(maxId);
            return product;
        }


        public bool FindEmployeeByID(int teudatZeut)
        {
            Employee E = itsDAL.FindEmployeeByID(teudatZeut);
            if (((Person)E).teudatZeut == 0)
            {
                return false;
            }
            else return true;
        }

        public bool FindClubMemberByID(int teudatZeut)
        {
            ClubMember C = itsDAL.FindClubMemberByID(teudatZeut);
            if (((Person)C).teudatZeut == 0)
            {
                return false;
            }
            else return true;
        }
        public void addUser(User U)
        {
            itsDAL.AddUserList(U);

        }

        public void removeUser(User U)
        {
            itsDAL.RemoveUserList(U);

        }

        public bool FindTransactionByID(string transactionID)
        {
            Transaction T = itsDAL.FindTransactionByID(transactionID);
            if (T.TransactionID == null)
            {
                return false;
            }
            else return true;
        }

        public bool FindDepartmentByID(int departmentID)
        {
            Department D = itsDAL.FindDepartmentById(departmentID);
            if (D.DepartmentID == 0)
            {
                return false;
            }
            else return true;
        }

        public List<Product> GetAllProductsList()
        {
            return itsDAL.GetProductList();
        }

        public List<User> GetAllUsers()
        {
            return itsDAL.GetUserList();
        }

        public List<ClubMember> GetAllClubMembers()
        {
            return itsDAL.GetClubMemberList();
        }

        public List<Employee> GetAllEmployees()
        {
            return itsDAL.GetEmployeeList();
        }

        public List<Transaction> GetAllTransactions()
        {
            return itsDAL.GetTransactionList();
        }

        public List<Department> GetAllDepartments()
        {
            return itsDAL.GetDepartmentList();
        }

        public void AddnewProduct(string name, productTypes Type, int inveID, int location, bool inStock, int stockCount, double price)
        {
            List<Product> productList = new List<Product>();
            Product P = new Product(name, Type, inveID, location, inStock, stockCount, price);
            itsDAL.AddProduct(P);
        }

        public void AddnewClubMember(int memberID, DateTime dateOfBirth, int teudatZeut, string firstName, string lastName, string gender, User user, DateTime creditCardExpiration, long creditCardNumber, int creditCardThreeDigits)
        {
            List<ClubMember> clunMemberList = new List<ClubMember>();
            ClubMember C = new ClubMember(memberID, dateOfBirth, teudatZeut, firstName, lastName, gender, user, creditCardExpiration, creditCardNumber, creditCardThreeDigits);
            itsDAL.AddClubMember(C);
        }

        public Transaction AddnewTransaction(DateTime dateTime, bool isAReturn, PaymentMethod paymentMethod, Dictionary<int, BL_backend.Transaction.SoldProducts> receipt)
        {
            Transaction T = new Transaction(dateTime, isAReturn, paymentMethod, receipt);
            itsDAL.AddTransaction(T);
            return T;
        }

        public void AddNewEmployee(int salary, int departmentID, int supervisor, int teudatZeut, string firstName, string lastName, string gender)
        {
            List<Employee> employeeList = new List<Employee>();
            Employee E = new Employee(salary, departmentID, supervisor, teudatZeut, firstName, lastName, gender);
            itsDAL.AddEmployee(E);
        }

        public void AddNewDepartment(int departmentID, string name)
        {
            List<Department> departmentList = new List<Department>();
            Department D = new Department(name, departmentID);
            itsDAL.AddDepartment(D);
        }
        public void deleteProduct(int inventoryId)
        {
            Product P = itsDAL.FindProductByID(inventoryId);
            itsDAL.deleteProductFromList(P);
        }
        public void AddNewTransactionId(int ClubSSN, string tranId)
        {
            itsDAL.AddNewTransactionId(ClubSSN, tranId);
        }

        public void deleteEmployee(int teudatZeut)
        {
            Employee E = itsDAL.FindEmployeeByID(teudatZeut);
            itsDAL.deleteEmployeeFromList(E);
        }

        public void deleteClubMember(int teudatZeut)
        {
            ClubMember C = itsDAL.FindClubMemberByID(teudatZeut);
            itsDAL.deleteClubMemberFromList(C);
        }

        public void deleteTransaction(string transactionID)
        {
            Transaction T = itsDAL.FindTransactionByID(transactionID);
            itsDAL.deleteTransactionFromList(T);
        }

        public void deleteDepartment(int departmentID)
        {
            Department D = itsDAL.FindDepartmentById(departmentID);
            itsDAL.deleteDepatmentFromList(D);
        }

        /*   public void updateProduct(int inventoryId, int stockCount)
           {
               itsDAL.updateProduct(inventoryId, stockCount, productTypes.Furniture, 0, 0, 0);

           }
           */
        public void updateUser(string userName, int pass, string NuserName, User.userType type)
        {
            itsDAL.updateUser(userName, pass, NuserName, type);

        }
        public void updateClubMember(int teudatZeut, int newTeu, string fName, string Lname, DateTime dateOfBirth, string gender, int memberID, DateTime cardExp, long cardNum, int cardThreeDig)
        {
            itsDAL.updateClubMemner(teudatZeut, newTeu, fName, Lname, dateOfBirth, gender, memberID, cardExp, cardNum, cardThreeDig);

        }
        public void updateEmployee(int teudatZeut, string fName, double salary, string Lname, string gender, int newTeu, int departmentID, int supervisor)
        {
            itsDAL.updateEmployee(teudatZeut, fName, salary, Lname, gender, newTeu, departmentID, supervisor);

        }

        public void updateDepartment(int departmentId, string name, int newDepId)
        {
            itsDAL.updateDepartment(departmentId, name, newDepId);

        }

        public void updateTransaction(string transactionID, bool isReturn, DateTime dateTime, PaymentMethod paymentMethod, string newTranId)
        {
            itsDAL.updateTransaction(transactionID, isReturn, dateTime, paymentMethod, newTranId);

        }

        public List<Employee> GetAllEmployeesBySalaryRange(double salary1, double salary2)
        {
            return itsDAL.GetEmployeeListBySalary(salary1, salary2);
        }

        public List<ClubMember> GetClubMemberListByDateOfBirth(DateTime Age1, DateTime Age2)
        {
            return itsDAL.GetClubMemberListByDateOfBirth(Age1, Age2);
        }


        public List<ClubMember> GetAllClubMemberByClubID(int MemberID)
        {
            List<ClubMember> CM;
            CM = itsDAL.GetClubMemberByClubID(MemberID);

            return CM;
        }

        public List<ClubMember> GetAllClubMemberBySSN(int tz)
        {
            List<ClubMember> CM;
            CM = itsDAL.GetAllClubMemberBySSN(tz);

            return CM;
        }

        public List<Employee> GetAllEmployeeListByDepID(int dpID)
        {
            List<Employee> Em = new List<Employee>();
            Em = itsDAL.GetEmployeeListByDepID(dpID);

            return Em;

        }

        public List<Transaction> GetAllTransactionListByDate(DateTime date)
        {
            List<Transaction> Tr = new List<Transaction>();
            Tr = itsDAL.GetTransactiontListIsDate(date);

            return Tr;
        }

        public List<Transaction> GetTransactionListByPaymentMethod(PaymentMethod PaymentMethod)
        {
            List<Transaction> Tr = new List<Transaction>();
            Tr = itsDAL.GetTransactionListByPaymentMethod(PaymentMethod);

            return Tr;
        }
        public List<Product> GetProductListByType(Enum type)
        {
            List<Product> P = new List<Product>();
            P = itsDAL.GetProductListByType(type);

            return P;
        }

        public List<User> FindUserByType(Enum type)
        {
            List<User> U = new List<User>();
            U = itsDAL.FindUserByType(type);

            return U;
        }

        public List<Product> GetProductListByPrice(double price1, double price2)
        {
            List<Product> P = new List<Product>();
            P = itsDAL.GetProductListByPrice(price1, price2);

            return P;
        }
        public List<Department> GetDepartmentByName(string name)
        {
            List<Department> D = new List<Department>();
            D = itsDAL.GetDepartmentByName(name);

            return D;
        }

        public int GetStockCountOfProduct(int inventoryID)
        {
            int counter = itsDAL.GetStockCountOfProduct(inventoryID);
            return counter;
        }




        public Product GetProductByName(string name)
        {
            List<Product> products = itsDAL.GetProductList();
            foreach (Product product in products)
            {
                if (product.getName().ToLower().Equals(name.ToLower()))
                {
                    return product;
                }

            }
            return null;
        }


        public Product GetProductByID(int id)
        {
            List<Product> productList = GetAllProductsList();
            foreach (Product pro in productList)
            {
                int prodId = pro.getID();
                if (prodId == id)
                {
                    return pro;
                }

            }
            return null;
        }

        public ClubMember GetClubMemberByUser(User user)
        {
            List<ClubMember> clubList = GetAllClubMembers();
            foreach (ClubMember club in clubList)
            {
                if (club.user.Equals(user))
                {
                    return club;
                }

            }
            return null;
        }


        public bool IsCreditCard(PaymentMethod payment)
        {
            switch (payment)
            {
                case PaymentMethod.Visa:
                case PaymentMethod.MasterCard:
                case PaymentMethod.AmericanExpress:
                case PaymentMethod.Isracard:
                    return true;
                default:
                    break;
            }
            return false;

        }

        public void updateProduct(int inventoryId, int stockCount, productTypes type, int location, double price, int newInv, bool inStock)
        {
            itsDAL.updateProduct(inventoryId, stockCount, type, location, price, newInv, inStock);
        }
    }

}