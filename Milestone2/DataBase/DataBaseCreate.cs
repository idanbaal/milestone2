﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BL_backend;
using System.IO;


namespace DAL
{
    class DataBaseCreate
    {
        /*  public List<Product> productList;
          public List<Employee> employeeList;
          public List<ClubMember> clubMemberList;
          public List<Transaction> transactionList;
          public List<User> userList;
          public List<Department> departmentList;

         */

        public DataBaseCreate()

        {
            string wanted_path = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));

            if (!File.Exists(wanted_path+"Employee.txt"))
            {
                List<Employee> employees = new List<Employee>();
                employees.Add(new Employee(4500, 141510000, 305509069, 305473488, "David", "Choen", "Male"));
                employees.Add(new Employee(4300, 141610000, 304789451, 306472488, "Nir", "Barnes", "Male"));
                employees.Add(new Employee(4000, 140510000, 304789451, 304789111, "Bar", "Zaguri", "Female"));
                employees.Add(new Employee(4000, 140510000, 304780001, 304789451, "Racheli", "Zaguri", "Female"));
                employees.Add(new Employee(10000, 140510000, 305509006, 304789451, "Yarden", "Chen", "Female"));
                StoreEmployee.Store(employees);
            }


            if (!File.Exists(wanted_path+"User.txt"))
            {
                List<User> users = new List<User>();
                users.Add(new User("Idan", 1234,User.userType.Admin));
                users.Add(new User("Yarden", 4321,User.userType.Manager));
                users.Add(new User("Racheli", 1111, User.userType.Worker));
                users.Add(new User("Avi", 2222, User.userType.Customer));
                StoreUser.Store(users);
            }


            if (!File.Exists(wanted_path+"Product.txt"))
            {
                List<Product> products = new List<Product>();
                products.Add(new Product("TV LED 32'' ", productTypes.Electronics, 301550906, 401510000, true, 500, 1500));
                products.Add(new Product("Broom", productTypes.Cleaning, 301610000, 401610000, true, 350, 20));
                products.Add(new Product("Couch 3 Seats", productTypes.Furniture, 301710000, 401710000, true, 500, 1500));
                StoreProduct.Store(products);
            }


            if (!File.Exists(wanted_path+"Department.txt"))
            {
                List<Department> departments = new List<Department>();
                departments.Add(new Department("Warehousemans", 141450000));
                departments.Add(new Department("Furniture Salon", 141610000));
                departments.Add(new Department("Cashiers", 140510000));
                StoreDepartment.Store(departments);
            }


            if (!File.Exists(wanted_path+"Transaction.txt"))
            {
                List<Product> products = StoreProduct.ImportProduct();
                int count = products.Count();
                List<Transaction> transactions = new List<Transaction>();
                BL_backend.Transaction.SoldProducts sol = new BL_backend.Transaction.SoldProducts(products[count - 1], 1);
                Dictionary<int, BL_backend.Transaction.SoldProducts> soldPro = new Dictionary<int, BL_backend.Transaction.SoldProducts> { { 1, sol } };
                transactions.Add(new Transaction(new DateTime(2015, 4, 15), false, PaymentMethod.Cash, soldPro));
                count--;
                if (count >= 0)
                {
                    BL_backend.Transaction.SoldProducts sol1 = new BL_backend.Transaction.SoldProducts(products[count], 3);
                    Dictionary<int, BL_backend.Transaction.SoldProducts> soldPro1 = new Dictionary<int, BL_backend.Transaction.SoldProducts>() { { 1, sol1 } };
                    transactions.Add(new Transaction(new DateTime(2015, 4, 18), false, PaymentMethod.Visa, soldPro1));
                    count--;
                }
                if (count >= 0)
                {
                    BL_backend.Transaction.SoldProducts sol2 = new BL_backend.Transaction.SoldProducts(products[count], 2);
                    Dictionary<int, BL_backend.Transaction.SoldProducts> soldPro2 = new Dictionary<int, BL_backend.Transaction.SoldProducts>() { { 1, sol }, { 2, sol2 } };
                    transactions.Add(new Transaction(new DateTime(2015, 4, 12), false, PaymentMethod.Check, soldPro2));
                    StoreTransaction.Store(transactions);
                }
            }


            if (!File.Exists(wanted_path + "ClubMember.txt"))
            {
                List<ClubMember> clubMembers = new List<ClubMember>();
                clubMembers.Add(new ClubMember(121212121, new DateTime(1984, 6, 14), 909090909, "Avi", "Liberman", "Male", new User("Avi", 5555, User.userType.ClubMember), new DateTime(2015, 5, 1), 123456789111, 234));
                clubMembers.Add(new ClubMember(136570000, new DateTime(1965, 1, 1), 140987065, "Rachel", "Levi", "Female", new User("Rachel", 4444, User.userType.ClubMember), new DateTime(2015, 5, 1), 123454545123, 155));
                clubMembers.Add(new ClubMember(111490000, new DateTime(1970, 4, 15), 200475982, "Ben", "Golan", "Male", new User("Ben", 9999, User.userType.ClubMember), new DateTime(2015, 4, 1), 123451545115, 233));
                clubMembers.Add(new ClubMember(121450000, new DateTime(1966, 5, 3), 130258976, "Yakov", "Mor", "Male", new User("Yakov", 8888, User.userType.ClubMember), new DateTime(2014, 1, 1), 1524516541222, 155));

                StoreClubMember.Store(clubMembers);
            }

        }


    }
}
