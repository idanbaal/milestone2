﻿using BL_backend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IDAL
    {
        void deleteProductFromList(Product P);

        void deleteEmployeeFromList(Employee E);

        void deleteClubMemberFromList(ClubMember C);

        void deleteTransactionFromList(Transaction T);

        void deleteDepatmentFromList(Department D);

        List<Product> GetProductList();

        void SetProductrList(List<Product> Product);

        List<Employee> GetEmployeeList();

        void SetEmployeeList(List<Employee> Employee);

        List<ClubMember> GetClubMemberList();

        void SetClubMemberList(List<ClubMember> ClubMember);

        List<Transaction> GetTransactionList();

        void SetTransactionList(List<Transaction> Transaction);

        List<User> GetUserList(List<User> User);

        void SetDepartmentList(List<Department> Department);

        List<Department> GetDepartmentList();

        List<User> GetUserList();

        ClubMember GetClubMemberByUser(string userName);

        void SetUserList(List<User> User);

        void AddProduct(Product P);

        void AddEmployee(Employee E);

        void AddClubMember(ClubMember C);

        Transaction AddTransaction(Transaction T);

        void AddUserList(User U);

        void AddDepartment(Department D);

        void RemoveUserList(User U);

        void updateUser(string userName, int pass, string NuserName, User.userType type);

        void updateProduct(int inventoryId, int stockCount, productTypes type, int location, double price, int newInv, bool inStock);

        void updateClubMemner(int teudatZeut, int newTeu, string fName, string Lname, DateTime dateOfBirth, string gender, int memberID, DateTime creditCardExpiration, long creditCardNumber, int creditCardThreeDigits);

        void updateEmployee(int teudatZeut, string fName, double salary, string Lname, string gender, int newTeu, int departmentID, int supervisor);

        void updateDepartment(int departmentId, string name, int newDepId);

        void updateTransaction(string transactionID, bool isReturn, DateTime dateTime, PaymentMethod paymentMethod, string newTranId);

        void AddNewTransactionId(int ClubSSN, string tranId);

        Product FindProductByID(int inventoryID);

        Employee FindEmployeeByID(int teudatZeut);

        ClubMember FindClubMemberByID(int teudatZeut);

        List<Employee> FindManagerByID(int TZ);

        Employee getEmp(string userName);

        Transaction FindTransactionByID(string transactionID);

        Department FindDepartmentById(int departmentId);

        List<Product> GetProductListByType(Enum type);// DONE

        List<User> FindUserByType(Enum type);

        Enum FindUserType(string user, int pass);

        List<Transaction> GetTransactiontListByMonth(int month);

        List<Product> GetProductListByPrice(double price1, double price2);// DONE

        int GetStockCountOfProduct(int inventoryID);

        List<Employee> GetEmployeeListByDepID(int dpID);//done

        List<Employee> GetEmployeeListBySalary(double Salary1, double Salary2);// done

        List<ClubMember> GetClubMemberByClubID(int MemberID);//done

        List<ClubMember> GetClubMemberListByDateOfBirth(DateTime DT1, DateTime DT2);// done

        List<Transaction> GetTransactionListByPaymentMethod(PaymentMethod PaymentMethod);

        List<Transaction> GetTransactiontListIsDate(DateTime date);

        List<Department> GetDepartmentByName(string name);

        string GetEmpGender(int SSN);

        bool IsUserExist(User user);

        List<ClubMember> GetAllClubMemberBySSN(int tz);

    }
}