﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_backend;

namespace DAL
{
    public class LINQ_DAL : IDAL
    {

        public List<Product> productList;
        public List<Employee> employeeList;
        public List<ClubMember> clubMemberList;
        public List<Transaction> transactionList;
        public List<User> userList;
        public List<Department> departmentList;

        public LINQ_DAL()
        {
            DataBaseCreate DB = new DataBaseCreate();
            productList = StoreProduct.ImportProduct();
            employeeList = StoreEmployee.ImportEmployee();
            clubMemberList = StoreClubMember.ImportClubMember();
            transactionList = StoreTransaction.ImportTransaction();
            userList = StoreUser.ImportUser();
            departmentList = StoreDepartment.ImportDepartment();

        }

        public List<Product> GetProductList()
        {
            return productList;
        }

        public List<User> GetUserList()
        {
            return userList;
        }

        public void SetProductrList(List<Product> Product)
        {
            productList = Product;
        }

        public List<Employee> GetEmployeeList()
        {
            return employeeList;
        }
        public void SetEmployeeList(List<Employee> Employee)
        {
            employeeList = Employee;
        }

        public List<ClubMember> GetClubMemberList()
        {
            return clubMemberList;
        }

        public void SetClubMemberList(List<ClubMember> ClubMember)
        {
            clubMemberList = ClubMember;
        }

        public List<Transaction> GetTransactionList()
        {
            return transactionList;
        }
        public void SetTransactionList(List<Transaction> Transaction)
        {
            transactionList = Transaction;
        }

        public List<User> GetUserList(List<User> User)
        {
            return userList;
        }
        public void SetUserList(List<User> User)
        {
            userList = User;
        }

        public List<Department> GetDepartmentList()
        {
            return departmentList;
        }
        public void SetDepartmentList(List<Department> Department)
        {
            departmentList = Department;
        }

        public bool IsUserExist(User user)
        {
            if (userList.Any(u => (u.UserName == user.UserName) && (u.Password == user.Password)))
            {
                return true;
            }
            return false;
        }


        public void updateUser(string userName, int pass, string NuserName, User.userType type)
        {
            userList.First(u => u.UserName == userName).Password = pass;
            userList.First(u => u.UserName == userName).UserName = NuserName;
            userList.First(u => u.UserName == userName).Type = type;
            StoreUser.Store(userList);
        }

        public ClubMember GetClubMemberByUser(string userName)
        {

            foreach (ClubMember C in clubMemberList)
            {
                if (C.firstName.Equals(userName))
                {
                    return C;
                }

            }
            return null;
        }

        public Employee getEmp(string userName)
        {
            foreach (Employee E in employeeList)
            {
                if (E.firstName == userName)
                {

                    return E;
                }

            }
            return null;
        }



        public void AddNewTransactionId(int ClubSSN, string tranId)
        {
            ClubMember C = FindClubMemberByID(ClubSSN);
            C.transactionID.Add(tranId);
            StoreClubMember.Store(clubMemberList);
        }

        //public LINQ_DAL()
        //{
        //    /// some data to run tests
        //    /// 


        //  Product P2 = new Product("chair", productTypes.Furniture, 305509066,3, false, 0, 222.2);
        //  Product P1 = new Product("shoes", productTypes.Electronics, 193923456, 123456789, true, 100, 344.3);
        //  List<ClubMember> C2 = new List<ClubMember>();
        //  List<ClubMember> C1 = new List<ClubMember>();
        //  DateTime Da1 = new DateTime(2015, 1, 1);
        //  DateTime Da2 = new DateTime(2015, 2, 6);
        //  DateTime Da3 = new DateTime(2012, 7, 2);
        //  DateTime Da4 = new DateTime(2010, 1, 1);
        //  DateTime Da5 = new DateTime(2015, 12, 15);
        //  DateTime Da6 = new DateTime(2015, 2, 4);
        //  ClubMember C3 = new ClubMember(3055, 2015/12/15, 305509069, "Yarden", "Chen", "female");
        //  ClubMember C4 = new ClubMember(3045, 2015 / 12 / 16, 305509064, "Ofir", "Chen", "female");
        //  Transaction T = new Transaction(Da2, false,PaymentMethod.Cash,null);
        //  Transaction T2 = new Transaction(Da3, true, PaymentMethod.Check,null);
        //  C2.Add(C3); C1.Add(C4);

        //  Employee E = new Employee(10000,234, 3045, 305509069, "Yarden", "Chen", "female");
        //  Employee E2 = new Employee(2000,345,305509069,235,"Guy", "Kr", "male");

        //  User U = new User ("admin", 1234);
        //  User U2 = new User("Yarden", 1512);

        //  productList = new List<Product>();
        //  transactionList = new List<Transaction>();
        //  clubMemberList = new List<ClubMember>();
        //  employeeList = new List<Employee>();
        //  userList = new List<User>();



        //  ////adding the data to the lists...
        //  productList.Add(P1); 
        //  productList.Add(P2);
        //clubMemberList.Add(C3); clubMemberList.Add(C4);
        //  employeeList.Add(E); employeeList.Add(E2);
        //  transactionList.Add(T); transactionList.Add(T2);
        //  userList.Add(U); userList.Add(U2);
        //  //userList.Remove(U); 



        //}



        public void deleteEmployeeFromList(Employee E)
        {
            employeeList.Remove(E);
            StoreEmployee.Store(employeeList);
        }

        public void deleteClubMemberFromList(ClubMember C)
        {
            clubMemberList.Remove(C);
            StoreClubMember.Store(clubMemberList);
        }

        public void deleteProductFromList(Product P)
        {
            productList.Remove(P);
            StoreProduct.Store(productList);
        }

        public void deleteTransactionFromList(Transaction T)
        {
            transactionList.Remove(T);
            StoreTransaction.Store(transactionList);
        }

        public void deleteDepatmentFromList(Department D)
        {
            departmentList.Remove(D);
            StoreDepartment.Store(departmentList);
        }

        public void AddEmployee(Employee E)
        {
            employeeList.Add(E);
            StoreEmployee.Store(employeeList);
        }

        public void AddClubMember(ClubMember C)
        {
            clubMemberList.Add(C);
            StoreClubMember.Store(clubMemberList);
        }

        public void AddProduct(Product P)
        {
            productList.Add(P);
            StoreProduct.Store(productList);
        }

        public Transaction AddTransaction(Transaction T)
        {
            transactionList.Add(T);
            StoreTransaction.Store(transactionList);
            return T;
        }

        public void AddUserList(User U)
        {
            userList.Add(U);
            StoreUser.Store(userList);
        }

        public void RemoveUserList(User U)
        {
            userList.Remove(U);
            StoreUser.Store(userList);
        }

        public void AddDepartment(Department D)
        {
            departmentList.Add(D);
            StoreDepartment.Store(departmentList);
        }


        public void updateProduct(int inventoryId, int stockCount, productTypes type, int location, double price, int newInv, bool inStock)
        {
            productList.First(p => p.InventoryID1 == inventoryId).StockCount = stockCount;
            productList.First(p => p.InventoryID1 == inventoryId).InventoryID1 = newInv;
            productList.First(p => p.InventoryID1 == inventoryId).Location = location;
            productList.First(p => p.InventoryID1 == inventoryId).Price1 = price;
            productList.First(p => p.InventoryID1 == inventoryId).Type = type;
            productList.First(p => p.InventoryID1 == inventoryId).InStock1 = inStock;
            StoreProduct.Store(productList);
        }


        public Product FindProductByID(int inventoryID)
        {
            foreach (Product P in productList)
            {
                if (P.InventoryID1 == inventoryID)
                {

                    return P;
                }

            }
            return null;
        }


        public List<Employee> FindManagerByID(int TZ)
        {
            List<Employee> Em = new List<Employee>();
            foreach (Employee E in employeeList)
            {
                if (E.Supervisor == TZ)
                {
                    Em.Add(E);

                }

            }
            if (Em.Count == 0) return null;
            return Em;


        }

        public Enum FindUserType(string name, int password)
        {
            foreach (User U in userList)
            {
                if ((U.UserName == name) && (U.Password == password))
                {

                    return U.Type;
                }

            }
            return User.userType.Customer;
        }
        public void updateClubMemner(int teudatZeut, int newTeu, string fName, string Lname, DateTime dateOfBirth, string gender, int memberID, DateTime creditCardExpiration, long creditCardNumber, int creditCardThreeDigits)
        {
            clubMemberList.First(c => c.teudatZeut == teudatZeut).setFirstName(fName);
            clubMemberList.First(c => c.teudatZeut == teudatZeut).setLastName(Lname);
            clubMemberList.First(c => c.teudatZeut == teudatZeut).setDateOfBirth(dateOfBirth);
            clubMemberList.First(c => c.teudatZeut == teudatZeut).setGender(gender);
            clubMemberList.First(c => c.teudatZeut == teudatZeut).setMemberId(memberID);
            clubMemberList.First(c => c.teudatZeut == teudatZeut).setID(newTeu);
            clubMemberList.First(c => c.teudatZeut == teudatZeut).CreditCardExpiration = creditCardExpiration;
            clubMemberList.First(c => c.teudatZeut == teudatZeut).CreditCardNumber = creditCardNumber;
            clubMemberList.First(c => c.teudatZeut == teudatZeut).CreditCardThreeDigits = creditCardThreeDigits;

            StoreClubMember.Store(clubMemberList);
        }

        public void updateEmployee(int teudatZeut, string fName, double salary, string Lname, string gender, int newTeu, int departmentID, int supervisor)
        {
            employeeList.First(e => e.teudatZeut == teudatZeut).firstName = fName;
            employeeList.First(e => e.teudatZeut == teudatZeut).lastName = Lname;
            employeeList.First(e => e.teudatZeut == teudatZeut).Salary = salary;
            employeeList.First(e => e.teudatZeut == teudatZeut).gender = gender;
            employeeList.First(e => e.teudatZeut == teudatZeut).teudatZeut = newTeu;
            employeeList.First(e => e.teudatZeut == teudatZeut).DepartmentID = departmentID;
            employeeList.First(e => e.teudatZeut == teudatZeut).Supervisor = supervisor;
            StoreEmployee.Store(employeeList);
        }

        public void updateDepartment(int departmentId, string name, int newDepId)
        {
            departmentList.First(d => d.DepartmentID == departmentId).Name = name;
            departmentList.First(d => d.DepartmentID == departmentId).DepartmentID = newDepId;
            StoreDepartment.Store(departmentList);
        }

        public void updateTransaction(string transactionID, bool isReturn, DateTime dateTime, PaymentMethod paymentMethod, string newTranId)
        {
            transactionList.First(t => t.TransactionID == transactionID).IsAReturn = isReturn;
            transactionList.First(t => t.TransactionID == transactionID).TransactionID = newTranId;
            transactionList.First(t => t.TransactionID == transactionID).DateTime = dateTime;
            transactionList.First(t => t.TransactionID == transactionID).PaymentMethod = paymentMethod;
            StoreTransaction.Store(transactionList);
        }

        public List<Transaction> GetTransactiontListByMonth(int month)
        {
            List<Transaction> Tr = new List<Transaction>();
            foreach (Transaction T in transactionList)
            {
                if (T.getDateTime().Month == month)
                {
                    Tr.Add(T);
                }

            }
            return Tr;
        }

        public Employee FindEmployeeByID(int teudatZeut)
        {
            foreach (Employee E in employeeList)
            {
                if (((Person)E).teudatZeut == teudatZeut) // from person
                {
                    return E;
                }

            }
            return new Employee();
        }

        public ClubMember FindClubMemberByID(int teudatZeut)
        {
            foreach (ClubMember C in clubMemberList)
            {
                if (((Person)C).teudatZeut == teudatZeut)
                {
                    return C;
                }

            }
            return new ClubMember();
        }


        public Transaction FindTransactionByID(string transactionID)
        {
            foreach (Transaction T in transactionList)
            {
                if (T.TransactionID == transactionID)
                {
                    return T;
                }

            }
            return new Transaction();
        }


        public Department FindDepartmentById(int departmentID)
        {
            foreach (Department D in departmentList)
            {
                if (D.DepartmentID == departmentID)
                {
                    return D;
                }

            }
            return new Department();
        }
        public List<Employee> GetEmployeeListByDepID(int dpID)
        {
            List<Employee> Em = new List<Employee>();
            foreach (Employee E in employeeList)
            {
                if (E.DepartmentID == dpID)
                {
                    Em.Add(E);

                }

            }
            if (Em.Count == 0) return null;
            return Em;
        }


        public List<ClubMember> GetClubMemberByClubID(int MemberID)
        {
            List<ClubMember> CM = new List<ClubMember>();
            foreach (ClubMember C in clubMemberList)
            {
                if (C.memberID == MemberID)
                {
                    CM.Add(C);
                }

            }
            if (CM.Count == 0)
                return null;
            else
                return CM;
        }

        public List<ClubMember> GetAllClubMemberBySSN(int tz)
        {
            List<ClubMember> CM = new List<ClubMember>();
            foreach (ClubMember C in clubMemberList)
            {
                if (C.teudatZeut == tz)
                {
                    CM.Add(C);
                }

            }
            if (CM.Count == 0)
                return null;
            else
                return CM;
        }

        public List<Employee> GetEmployeeListBySalary(double salary1, double salary2)
        {
            List<Employee> DL = new List<Employee>();
            foreach (Employee E in employeeList)
            {
                if (E.Salary > salary1 && E.Salary < salary2)
                {
                    DL.Add(E);
                }

            }
            if (DL.Count == 0) return null;
            return DL;

        }


        public List<Transaction> GetTransactionListByPaymentMethod(PaymentMethod PaymentMethod)
        {
            List<Transaction> Tr = new List<Transaction>();
            foreach (Transaction T in transactionList)
            {
                if (T.PaymentMethod == PaymentMethod)
                {
                    Tr.Add(T);
                }

            }
            if (Tr.Count == 0) return null;
            return Tr;
        }

        public List<Transaction> GetTransactiontListIsDate(DateTime date)
        {
            List<Transaction> Tr = new List<Transaction>();
            foreach (Transaction T in transactionList)
            {
                if (T.DateTime == date)
                {
                    Tr.Add(T);
                }

            }
            if (Tr.Count == 0) return null;
            return Tr;
        }

        public List<ClubMember> GetClubMemberListByDateOfBirth(DateTime DT1, DateTime DT2)
        {
            List<ClubMember> CM = new List<ClubMember>();
            foreach (ClubMember C in clubMemberList)
            {
                if (C.dateOfBirth > DT1 && C.dateOfBirth < DT2)
                {
                    CM.Add(C);
                }

            }
            if (CM.Count == 0) return null;
            return CM;
        }


        public List<Product> GetProductListByPrice(double price1, double price2)
        {
            List<Product> PR = new List<Product>();
            foreach (Product P in productList)
            {
                if (P.Price1 > price1 && P.Price1 < price2)
                {
                    PR.Add(P);
                }

            }
            if (PR.Count == 0) return null;
            return PR;

        }


        public List<Product> GetProductListByType(Enum type)
        {
            List<Product> PR = new List<Product>();

            foreach (Product P in productList)
            {
                if (P.getType().ToString() == type.ToString())
                {
                    PR.Add(P);
                }

            }
            if (PR.Count == 0) return null;
            return PR;
        }


        public List<User> FindUserByType(Enum type)
        {
            List<User> User = new List<User>();

            foreach (User U in userList)
            {
                if (U.Type.ToString() == type.ToString())
                {
                    User.Add(U);
                }

            }
            if (User.Count == 0) return null;
            return User;
        }

        public List<Department> GetDepartmentByName(string name)
        {
            List<Department> DP = new List<Department>();

            foreach (Department D in departmentList)
            {
                if (D.Name.Equals(name))
                {
                    DP.Add(D);
                }

            }
            if (DP.Count == 0) return null;

            return DP;
        }

        public int GetStockCountOfProduct(int inventoryID)
        {
            int count = 0;
            foreach (Product P in productList)
            {
                if (P.InventoryID1 == inventoryID)
                {
                    count = count + P.StockCount;
                }

            }
            return count;
        }


        public string GetEmpGender(int SSN)
        {
            string g = "";


            employeeList.First(e => e.teudatZeut == SSN).gender = g;
            return g;
        }
    }
}