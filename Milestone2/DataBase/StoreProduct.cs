﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using BL_backend;

namespace DAL
{
    class StoreProduct
    {
        //(auxiliary funcation)store and encrypt the Product data in the hard disk
       public static void Store(List<Product> products)
        { 
            EncryptProduct(products, InitializeKeyIV.Key, InitializeKeyIV.IV);
        }
        //(auxiliary funcation)Import and decrypt the Product data from the hard disk
       public static List<Product> ImportProduct()
        {
             return DecryptProduct( InitializeKeyIV.Key, InitializeKeyIV.IV);
        }


        //Store and encrypt the Product data in the hard disk   
        static void EncryptProduct(List<Product> product , byte[] Key, byte[] IV)
            {
                string wanted_path = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
                using (var fs = new FileStream(wanted_path+"Product.txt", FileMode.Create, FileAccess.Write))
                using (Aes aesAlg = Aes.Create())
                {
                    aesAlg.Key = Key;
                    aesAlg.IV = IV;

                    //Create a encryptor to perform the stream transform.
                    ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                
                    using (var cryptoStream = new CryptoStream(fs, encryptor, CryptoStreamMode.Write))
                    {
                        BinaryFormatter formatter = new BinaryFormatter();

                         //This is where you serialize the class
                        formatter.Serialize(cryptoStream, product);
                    }
                }
            }


      //Import and decrypt the Product data from the hard disk
      static List <Product> DecryptProduct(byte[] Key, byte[] IV)
        {
            string wanted_path = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
            using (var fs = new FileStream(wanted_path+"Product.txt", FileMode.Open, FileAccess.Read))

            // Create an Aes object 
            // with the specified key and IV. 
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                using (var cryptoStream = new CryptoStream(fs, decryptor, CryptoStreamMode.Read))
                {
                    BinaryFormatter formatter = new BinaryFormatter();

                    // This is where you deserialize the class
                    return (List<Product>)formatter.Deserialize(cryptoStream);
                }
            }
        }
    }
}

    

