﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using BL_backend;



namespace DAL
{
    class StoreUser
    {
        //(auxiliary funcation)store and encrypt the User data in the hard disk
       public static void Store(List<User> users)
        {
            EncryptUser(users,InitializeKeyIV.Key, InitializeKeyIV.IV);
        }

        //(auxiliary funcation)Import and decrypt the User data from the hard disk
       public static List<User> ImportUser()
        {
            return DecryptUser(InitializeKeyIV.Key, InitializeKeyIV.IV);
        }


        //Store and encrypt the User data in the hard disk
        static void EncryptUser(List<User> user, byte[] Key, byte[] IV)
        {
            string wanted_path = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
            using (var fs = new FileStream(wanted_path+"User.txt", FileMode.Create, FileAccess.Write))
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                //Create a encryptor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                using (var cryptoStream = new CryptoStream(fs, encryptor, CryptoStreamMode.Write))
                {
                    BinaryFormatter formatter = new BinaryFormatter();

                    //This is where you serialize the class
                    formatter.Serialize(cryptoStream, user);
                }
            }
        }


        //Import and decrypt the User data from the hard disk
        static List<User> DecryptUser(byte[] Key, byte[] IV)
        {
            string wanted_path = Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()));
            using (var fs = new FileStream(wanted_path+"User.txt", FileMode.Open, FileAccess.Read))

            // Create an Aes object 
            // with the specified key and IV. 
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                using (var cryptoStream = new CryptoStream(fs, decryptor, CryptoStreamMode.Read))
                {
                    BinaryFormatter formatter = new BinaryFormatter();

                    // This is where you deserialize the class
                    return (List<User>)formatter.Deserialize(cryptoStream);
                }
            }
        }
    }
}

    


