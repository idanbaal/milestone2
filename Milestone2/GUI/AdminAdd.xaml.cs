﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_backend;

namespace GUI
{
    /// <summary>
    /// Interaction logic for Dor.xaml
    /// </summary>
    public partial class AdminAdd : Window
    {
        private IBL itsBL;
        private User U;
        public AdminAdd(IBL IBL, User U)
        {
            this.itsBL = IBL;
            this.U = U;
            InitializeComponent();
        }

        private void add_user_Click(object sender, RoutedEventArgs e)
        {
            int pass;

            try
            {
                string userName2 = userName.Text;
                pass = Convert.ToInt32(password.Text);
                User.userType type = (User.userType)Enum.Parse(typeof(User.userType), cbType.Text);
                User user = new User(userName2, pass, type);
                itsBL.addUser(user);
                 MessageBox.Show("The User added succfully");
            }
            catch
            {
                 MessageBox.Show("Invalid Input");
            }
    
        }

        private void p_create_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string Name = name.Text;
                productTypes typeP = (productTypes)Enum.Parse(typeof(productTypes), cbTypeP.Text);
                int invId = Convert.ToInt32(invID.Text);
                int depId = Convert.ToInt32(depID.Text);
                int stock = Convert.ToInt32(stockC.Text);
                double Price = Double.Parse(price.Text);


                itsBL.AddnewProduct(Name, typeP, invId, depId, true, stock, Price);
                MessageBox.Show("The Product added succfully");
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid Input");
            }
              
        }
        
        private void cbTypeP_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string FName = firstName.Text;
                string LName = lastName.Text;
                int Id = Convert.ToInt32(ID.Text);
                string IDd  = ID.Text;
                
                if (IDd.Length !=9)
                {
                    MessageBox.Show("Please enter a valid ID num (9 digits)");
                    return;
                }
                int deppId = Convert.ToInt32(departmentId.Text);
                int sal = Convert.ToInt32(salary.Text);
                int manId = Convert.ToInt32(manegerId.Text);
                string gender = cbGender.Text;

                itsBL.AddNewEmployee(sal, deppId, manId, Id, FName, LName, gender);
               MessageBox.Show("The Employee added succfully");
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid Input");
            }
                
        }

        private void d_create_Click(object sender, RoutedEventArgs e)
        {
            
                string name = DepName.Text;
                int x;
                bool success = int.TryParse(DepID.Text, out x);
                if (success)
                {
                    itsBL.AddNewDepartment(int.Parse(DepID.Text), DepName.Text);

                    MessageBox.Show("The Department added seccfully");

                }
                else
                {

                    MessageBox.Show("Invalid Input");
                }
            

         
        }

        private void c_create_Click(object sender, RoutedEventArgs e)
        {
           
            try
            {
                string Fname = firstNameC.Text;
                string Lname = lastNameC.Text;
                int IdC = Convert.ToInt32(idC.Text);
                string gender = cbGender.Text;
                DateTime date = (DateTime)dpC.SelectedDate;
                int clubID = Convert.ToInt32(clubMemberId.Text);
                int password = Convert.ToInt32(passC.Text);
                string user = userNameC.Text;
                User U = new User(user, password, User.userType.ClubMember);



                itsBL.AddnewClubMember(clubID, date, IdC, Fname, Lname, gender, U,date, 0, 0);
                MessageBox.Show("The Club Member added seccfully");
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid Input");
            }
        }

     

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            adminMain main = new adminMain(itsBL, U);
            main.Show();
            this.Close(); 
        }

        private void pbD_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }

       

       
    }
}