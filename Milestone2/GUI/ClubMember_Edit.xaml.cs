﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_backend;

namespace GUI
{
    /// <summary>
    /// Interaction logic for ClubMember_Edit.xaml
    /// </summary>
    public partial class ClubMember_Edit : Window
    {
        IBL itsBl;
        ClubMember toEdit;
        int lastTz;
        Window last;
        public ClubMember_Edit(ClubMember member, IBL bl, Window last)
        {
            this.last = last;

            InitializeComponent();
            toEdit = new ClubMember(member);
            itsBl = bl;
            lastTz = member.TeudatZeut;

            MembetId.Text = "" + member.MemberID;
            Date.Text = member.DateOfBirth.ToString();
            ID.Text = "" + member.TeudatZeut;
            FName.Text = member.FirstName;
            LName.Text = member.LastName;
            if (member.Gender.Equals("Male"))
            {
                Male.IsChecked = true;
            }
            else
            {
                Female.IsChecked = true;
            }



        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                toEdit.MemberID = Convert.ToInt32(MembetId.Text);
                toEdit.DateOfBirth = Convert.ToDateTime(Date.Text);
                toEdit.TeudatZeut = Convert.ToInt32(ID.Text);
                string IDd2 = ID.Text;

                if (IDd2.Length != 9)
                {
                    MessageBox.Show("Please enter a valid ID num (9 digits)");
                    return;
                }
                toEdit.FirstName = FName.Text;
                toEdit.LastName = LName.Text;
                if (Male.IsChecked == true)
                {
                    toEdit.Gender = "Male"; 
                    try
                    {
                        toEdit.MemberID = Convert.ToInt32(MembetId.Text);
                        toEdit.DateOfBirth = Convert.ToDateTime(Date.Text);
                        toEdit.TeudatZeut = Convert.ToInt32(ID.Text);
                        string IDd = ID.Text;

                        if (IDd.Length != 9)
                        {
                            MessageBox.Show("Please enter a valid ID num (9 digits)");
                            return;
                        }
                        toEdit.FirstName = FName.Text;
                        toEdit.LastName = LName.Text;
                        if (Male.IsChecked == true)
                        {
                            toEdit.Gender = "Male";
                        }
                        else
                        {
                            toEdit.Gender = "Female";
                        }
                    }

                    catch (Exception)
                    {
                        MessageBox.Show("Invalid Input");
                    }

                    itsBl.updateClubMember(lastTz, toEdit.TeudatZeut, toEdit.FirstName, toEdit.LastName, toEdit.DateOfBirth, toEdit.Gender, toEdit.MemberID, toEdit.CreditCardExpiration, toEdit.CreditCardNumber, toEdit.CreditCardThreeDigits);
                    MessageBox.Show("Club Member was Updated Successfully");



                }
                else
                {
                    toEdit.Gender = "Female";
                }
            }

            catch (Exception)
            {
                MessageBox.Show("Invalid Input");
            }

            itsBl.updateClubMember(lastTz, toEdit.TeudatZeut, toEdit.FirstName, toEdit.LastName, toEdit.DateOfBirth, toEdit.Gender, toEdit.MemberID, toEdit.CreditCardExpiration, toEdit.CreditCardNumber, toEdit.CreditCardThreeDigits);
            MessageBox.Show("Club Member was Updated Successfully");




        }

        private void btnGoBack_Click(object sender, RoutedEventArgs e)
        {
           
            last.Show();
            this.Close();

        }
    }
}