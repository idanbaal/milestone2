﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_backend;

namespace GUI
{
    /// <summary>
    /// Interaction logic for Data_View.xaml
    /// </summary>
    public partial class Edit_Entities : Window
    {
        Window last;
        IBL itsBL;
        ClubMember member;
        Product product;
        Employee employee;
        User user;
        Department dp;
        Transaction tran;
        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        public Edit_Entities(IBL bl , Window l)
        {
            last = l;
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0,0,0,0,120);
            dispatcherTimer.Start();

            itsBL = bl;
            InitializeComponent();

            
            
        //    gridy_employee.ItemsSource = itsBL.GetAllEmployees();
            
            
            //ProductsTab.Content = tabFrame;
        }
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            progBar.Value = Convert.ToInt32(progBar.Value) + 10;
            
            if (Convert.ToInt32(progBar.Value) > 90)
            {
                dispatcherTimer.Stop();
                switch (tabControl.SelectedIndex)
                {
                    case 0:
                        gridy.ItemsSource = itsBL.GetAllClubMembers();
                        break;

                    case 1:
                        gridy_product.ItemsSource = itsBL.GetAllProductsList();
                        break;

                    case 2:
                        gridy_employee.ItemsSource = itsBL.GetAllEmployees();
                        break;

                    case 3:
                        gridy_user.ItemsSource = itsBL.GetAllUsers();
                        break;

                    case 4:
                        gridyDp.ItemsSource = itsBL.GetAllDepartments();
                        break;

                    case 5:
                        gridyTran.ItemsSource = itsBL.GetAllTransactions();
                        break;
                }
  
            }
        }

        private void gridy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            member = (ClubMember)gridy.SelectedItem;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (member == null)
            {
                MessageBox.Show("You Didn't Select Line");
                return;
            }
            else
            {
                ClubMember_Edit edit = new ClubMember_Edit(member, itsBL, this);
                Hide();
                edit.Show();
            
            }

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

            if (member == null)
            {
                MessageBox.Show("You Didn't Select Line");
                return;
            }
            else
            {
                itsBL.deleteClubMember(member.TeudatZeut);
                MessageBox.Show("The Selected Club Member was Deleted Successfully");
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {

            if (product == null)
            {
                MessageBox.Show("You Didn't Select Line");
                return;
            }
            else
            {
                Product_Edit edit = new Product_Edit(product, itsBL, this);
                Hide();
                edit.Show();
            }
        }

        private void gridy_product_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            product =(Product) gridy_product.SelectedItem;
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            if (product == null)
            {
                MessageBox.Show("You Didn't Select Line");
                return;
            }
            else
            {
                itsBL.deleteProduct(product.InventoryID1);
                MessageBox.Show("The Selected Product was Deleted Successfully");
            }
        }

        private void editEmpoyee(object sender, RoutedEventArgs e)
        {

            if (employee == null)
            {
                MessageBox.Show("You Didn't Select Line");
                return;
            }
            else
            {
                Employee_Edit edit = new Employee_Edit(employee, itsBL, this);
                Hide();
                edit.Show();
            }
        }

        private void gridy_employe_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            employee = (Employee)gridy_employee.SelectedItem;
        }

        private void removeEmployee(object sender, RoutedEventArgs e)
        {
            if (employee == null)
            {
                MessageBox.Show("You Didn't Select Line");
                return;
            }
            else
            {
                itsBL.deleteEmployee(employee.TeudatZeut);
                MessageBox.Show("The Selected Product was Deleted Successfully");
            }
        }

        private void ProgressBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            progBar.Value = 0;
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0,0,0,0,120);
            dispatcherTimer.Start();
            
        }

        private void editUsers(object sender, RoutedEventArgs e)
        {

            if (user == null)
            {
                MessageBox.Show("You Didn't Select Line");
                return;
            }
            else
            {
                User_Edit edit = new User_Edit(user, itsBL, this);
                Hide();
                edit.Show();
            }
        }

        private void removeusers(object sender, RoutedEventArgs e)
        {

            if (user == null)
            {
                MessageBox.Show("You Didn't Select Line");
                return;
            }
            else
            {
                itsBL.removeUser(user);
                MessageBox.Show("The Selected Product was Deleted Successfully");
            }
        }

        private void gridy_users(object sender, SelectionChangedEventArgs e)
        {
            user = (User)gridy_user.SelectedItem;
        }

        private void gridtDp_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dp =(Department) gridyDp.SelectedItem;
        }

        private void editdp(object sender, RoutedEventArgs e)
        {
            if (dp == null)
            {
                MessageBox.Show("You Didn't Select Line");
                return;
            }
            else
            {
                Department_Edit edit = new Department_Edit(dp, itsBL, this);
                Hide();
                edit.Show();
            }
        }

        private void removedp(object sender, RoutedEventArgs e)
        {
            if (dp == null)
            {
                MessageBox.Show("You Didn't Select Line");
                return;
            }
            else
            {
                itsBL.deleteDepartment(dp.DepartmentID);
                MessageBox.Show("The Selected Department was Deleted Successfully");
            }
        }

        private void editTran(object sender, RoutedEventArgs e)
        {
            if (tran == null)
            {
                MessageBox.Show("You Didn't Select Line");
                return;
            }
            else
            {
                Transaction_Edit edit = new Transaction_Edit(tran, itsBL, this);
                Hide();
                edit.Show();
            }
        }

        private void removeTran(object sender, RoutedEventArgs e)
        {
            if (tran == null)
            {
                MessageBox.Show("You Didn't Select Line");
                return;
            }
            else
            {
                itsBL.deleteTransaction(tran.TransactionID);
                MessageBox.Show("The Selected Transaction was Deleted Successfully");
            }
        }

        private void gridtTran_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            tran = (Transaction)gridyTran.SelectedItem;
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            last.Show();
            this.Close();

        }

      

    }
}
