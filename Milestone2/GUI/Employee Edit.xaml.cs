﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_backend;

namespace GUI
{
    /// <summary>
    /// Interaction logic for Employee_Edit.xaml
    /// </summary>
    public partial class Employee_Edit : Window
    {
        IBL itBL;
        Employee toedit;
        int lastTz;
        Window last;
        public Employee_Edit(Employee employee , IBL bl, Window last)
        {
            itBL = bl;
            toedit = employee;
            this.last = last;
            InitializeComponent();

            toedit = new Employee(employee);
         
            lastTz = employee.TeudatZeut;
            superID.Text = "" + employee.Supervisor;
            Salary.Text = "" + employee.Salary;
            dpId.Text =""+ employee.DepartmentID;
            ID.Text = "" + employee.TeudatZeut;
            FName.Text = employee.FirstName;
            LName.Text = employee.LastName;
            if (employee.Gender.Equals("Male"))
            {
                Male.IsChecked = true;
            }
            else
            {
                Female.IsChecked = true;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                toedit.Salary = Convert.ToDouble(Salary.Text);
                toedit.DepartmentID = Convert.ToInt32(dpId.Text);
                toedit.Supervisor = Convert.ToInt32(superID.Text);
                toedit.TeudatZeut = Convert.ToInt32(ID.Text);
                toedit.FirstName = FName.Text;
                toedit.LastName = LName.Text;
                if (Male.IsChecked == true)
                {
                    toedit.Gender = "Male";
                }
                else
                {
                    toedit.Gender = "Female";
                }
            }

            catch (Exception)
            {
                MessageBox.Show("Invalid Input");
            }

            itBL.updateEmployee(lastTz, toedit.FirstName, toedit.Salary, toedit.LastName, toedit.Gender, toedit.TeudatZeut, toedit.DepartmentID, toedit.Supervisor);
            MessageBox.Show("Employee was Updated Successfully");



        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            last.Show();
            this.Close();
        }
    }
}
