﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL_backend;
using BL;

namespace GUI
{
    /// <summary>
    /// Interaction logic for Product_Edit.xaml
    /// </summary>
    public partial class Product_Edit : Window
    {
        IBL itsBL;
        Product toedit;
        int inventory;
        Window last;
        public Product_Edit(Product product, IBL bl, Window last)
        {
            this.last = last;
            InitializeComponent();
            itsBL = bl;
            toedit = new Product(product);
            inventory = product.InventoryID1;
            Name.Text = toedit.Name;
            Inventory.Text = "" + toedit.InventoryID1;
            Location.Text = "" + toedit.Location;
            InStock.IsChecked = toedit.InStock1;
            StockCount.Text = ""+toedit.StockCount;
            Price.Text = "" + toedit.Price1;
            Type.Text = "" + toedit.Type;
         
           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                toedit.Name = Name.Text;
                toedit.InventoryID1 = Convert.ToInt32(Inventory.Text);
                toedit.Location = Convert.ToInt32(Location.Text);
                toedit.InStock1 = InStock.IsChecked.Value;
                toedit.StockCount = Convert.ToInt32(StockCount.Text);
                toedit.Price1 = Convert.ToDouble(Price.Text);
                toedit.Type = (productTypes)Enum.Parse(typeof(productTypes), Type.Text, true);

            }
            catch (Exception)
            {
                MessageBox.Show("Invalid Input");
            }

            itsBL.updateProduct(inventory, toedit.StockCount, toedit.Type, toedit.Location, toedit.Price1, toedit.InventoryID1,toedit.InStock1);
            MessageBox.Show("The Product was Updated Successfully");
        }

        private void Type_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            last.Show();
            this.Close();
        }
    }
}
