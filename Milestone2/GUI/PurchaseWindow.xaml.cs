﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using DAL;
using BL_backend;
using System.Threading;
using System.Collections;

namespace GUI
{
    /// <summary>
    /// Interaction logic for PurchaseWindow.xaml
    /// </summary>
    public partial class PurchaseWindow : Window
    {

        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        IBL _ibl;
        User _user;

        /*   public PurchaseWindow()
           {
               InitializeComponent();
               _ibl = new product_BL(new LINQ_DAL());
               //  _user = new User("Avi", 2222, User.userType.Customer);
               //_user = new User("Idan", 1234, User.userType.Admin);
              // _user = new User("Avi", 5555, User.userType.ClubMember);
               // _user = new User("Rachel", 4444, User.userType.ClubMember);
               InitWindow(_ibl, _user);

           }*/


        public PurchaseWindow(IBL ibl, User user)
        {
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 120);
            dispatcherTimer.Start();

            InitializeComponent();
            InitWindow(ibl, user);

        }

        private void InitWindow(IBL ibl, User user)
        {
            this._ibl = ibl;
            this._user = user;
            string[] productTypes = Enum.GetNames(typeof(productTypes));
            cmbType.ItemsSource = productTypes;
            string[] paymentMethod = Enum.GetNames(typeof(PaymentMethod));
            cmbPay.ItemsSource = paymentMethod;
            LoadBestSeller();
            List<Product> productList = _ibl.GetAllProductsList();
            LoadProducts(productList);
            comboSelection();
            InitControls();

        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            progBar.Value = Convert.ToInt32(progBar.Value) + 10;

            if (Convert.ToInt32(progBar.Value) > 90)
            {
                dispatcherTimer.Stop();
                if (cmbType.SelectedItem != null)
                {
                    productTypes type = (productTypes)Enum.Parse(typeof(productTypes), (string)cmbType.SelectedItem);
                    List<Product> productList = _ibl.GetProductListByType(type);
                    LoadProducts(productList);

                }
            }
        }


        /*  private void InitControls()
          {
            
              btnAdminPurchase.IsEnabled = false;
              btnPay.IsEnabled = false;
              btnPayAndSave.IsEnabled = false;
              switch (_user.Type)
              {
                  case User.userType.Admin:
                      btnAdminPurchase.IsEnabled = true;
                      break;
                  case User.userType.Manager:
                      break;
                  case User.userType.Worker:
                      break;
                  case User.userType.Customer:
                      btnPay.IsEnabled = true;
                      break;
                  case User.userType.ClubMember:
                      btnPay.IsEnabled = true;
                      break;
                  default:
                      break;
              }
          }*/



        private void InitControls()
        {

            btnAdminPurchase.IsEnabled = false;
            btnPay.IsEnabled = false;
            btnPayAndSave.IsEnabled = false;
            if (_ibl.FindUserType(_user.UserName, _user.Password).Equals(User.userType.ClubMember))
            {
                btnPay.IsEnabled = true;
                btnPayAndSave.IsEnabled = true;
            }
            else if (_ibl.FindUserType(_user.UserName, _user.Password).Equals(User.userType.Admin))
            {
                btnAdminPurchase.IsEnabled = true;
            }
            else if (_ibl.FindUserType(_user.UserName, _user.Password).Equals(User.userType.Customer) || _ibl.FindUserType(_user.UserName, _user.Password).Equals(User.userType.Worker) || _ibl.FindUserType(_user.UserName, _user.Password).Equals(User.userType.Manager))
            {
                btnPay.IsEnabled = true;
            }
        }

        private void LoadProducts(List<Product> productList)
        {
            ListProduct.Items.Clear();
            if (productList == null)
            {
                productList = _ibl.GetAllProductsList();
            }

            foreach (Product product in productList)
            {
                ProductListItem item = new ProductListItem(product);

                ListProduct.Items.Add(item);

            }


        }

        private void LoadBestSeller()
        {
            Product product = _ibl.GetBestSeller(DateTime.Now.Month);
            if (product != null)
            {
                lblBestSeller.Content = product.getName();
            }
        }

        private void ListCart_Drop(object sender, DragEventArgs e)
        {

            // If the DataObject contains string data, extract it. 
            if (e.Data.GetDataPresent(typeof(ProductListItem)))
            {
                ProductListItem dataItem = (ProductListItem)e.Data.GetData(typeof(ProductListItem));
                ListCart.Items.Add(new ProductListItem(dataItem));

            }

        }

        private void ListProduct_MouseMove(object sender, MouseEventArgs e)
        {
            ProductListItem item = ListProduct.SelectedItem as ProductListItem;
            if (item != null && e.LeftButton == MouseButtonState.Pressed)
            {
                DragDrop.DoDragDrop(item,
                                     item,
                                     DragDropEffects.Copy);
            }
        }


        private void btnSearchName_Click(object sender, RoutedEventArgs e)
        {

            Product product = _ibl.GetProductByName(txtName.Text);
            ListProduct.Items.Clear();
            if (product != null)
            {
                ProductListItem item = new ProductListItem(product);

                ListProduct.Items.Add(item);
            }
            else if (String.IsNullOrEmpty(txtName.Text))
            {
                List<Product> productList = _ibl.GetAllProductsList();
                LoadProducts(productList);
            }
        }

        private void txtName_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            txtName.Text = "";
        }

        private void txtID_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            txtID.Text = "";
        }



        private void cmbType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 120);
            dispatcherTimer.Start();

        }


        private void txtThreeDigits_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            txtThreeDigits.Text = "";

        }

        private void txtCardNumber_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            txtCardNumber.Text = "";

        }

        private void btnSearchByID_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(txtID.Text))
            {
                List<Product> productList = _ibl.GetAllProductsList();
                LoadProducts(productList);
            }
            else
            {
                int n;

                bool success = int.TryParse(txtID.Text, out n);
                if (success)
                {
                    Product product = _ibl.GetProductByID(n);

                    ListProduct.Items.Clear();
                    if (product != null)
                    {
                        ProductListItem item = new ProductListItem(product);

                        ListProduct.Items.Add(item);
                    }
                }
                else
                {
                    MessageBox.Show("Invalid input");
                }
            }
        }

        public void comboSelection()
        {
            if (cmbPay.Text.Equals("cash") || cmbPay.Text.Equals("check") || cmbPay.Text.Equals("other"))
            {
                txtCardNumber.IsEnabled = false;
                txtYear.IsEnabled = false;
                txtMonth.IsEnabled = false;
                txtThreeDigits.IsEnabled = false;
            }
        }


        private void btnPayAndSave_Click(object sender, RoutedEventArgs e)
        {

            ClubMember clubMember = _ibl.GetClubMemberByUser(_user.UserName);
            if (clubMember != null && GetCreditDetails())
            {

                DateTime dateExp = new DateTime(int.Parse(txtYear.Text), int.Parse(txtMonth.Text), 1);
                clubMember.CreditCardExpiration = dateExp;
                long number = long.Parse(txtCardNumber.Text);
                clubMember.CreditCardNumber = number;
                int threeDig = int.Parse(txtThreeDigits.Text);
                clubMember.CreditCardThreeDigits = threeDig;
                _ibl.updateClubMember(clubMember.teudatZeut, clubMember.teudatZeut, clubMember.getFirstName(),
                clubMember.getLasttName(), clubMember.getDateOfBirth(), clubMember.getGender(),
                clubMember.memberID, clubMember.CreditCardExpiration, clubMember.CreditCardNumber,
                clubMember.CreditCardThreeDigits);
                PaymentMethod payment = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), (string)cmbPay.SelectedItem);
                Dictionary<int, Transaction.SoldProducts> receipt = GetReciept();
                if (receipt.Count > 0)
                {
                    Transaction tran = _ibl.AddnewTransaction(DateTime.Now, false, payment, receipt);

                    ClubMember club = _ibl.GetClubMemberByUser(_user.UserName);
                    _ibl.AddNewTransactionId(club.memberID, tran.TransactionID);
                    MessageBox.Show("Your card's information saved successfully and you paid successfully!");
                }
                else
                {
                    MessageBox.Show("Please choose products");
                }
            }
        }

        private Dictionary<int, Transaction.SoldProducts> GetReciept()
        {
            bool flag = false;
            Dictionary<int, Transaction.SoldProducts> reciept = new Dictionary<int, Transaction.SoldProducts>();
            foreach (ProductListItem item in ListCart.Items)
            {
                int quantity = GetQuantity(item);
                if (quantity > 0 && quantity <= item.StockCount)
                {
                    Transaction.SoldProducts soldProd = new Transaction.SoldProducts(item.Product, quantity);
                    reciept.Add(item.InventoryID, soldProd);
                    int stockCount = item.StockCount - quantity;
                    _ibl.updateProduct(item.InventoryID, stockCount, item.Type, item.Location, item.Price, item.InventoryID, true);
                    flag = true;
                }
                else
                {
                    MessageBox.Show("Invalid Quantity");
                }

            }
            if (flag)
            {
                return reciept;
            }
            else return null;
        }

        private double GetTotalPrice()
        {
            double result = 0;
            foreach (ProductListItem item in ListCart.Items)
            {
                int quantity = GetQuantity(item);
                result += quantity * item.Price;
            }
            return result;
        }

        private int GetQuantity(ProductListItem ListCartItem)
        {
            var gridView = ListCart.View as GridView;
            ListBoxItem myListBoxItem = ListCart.ItemContainerGenerator.ContainerFromItem(ListCartItem) as ListBoxItem;

            //ContentControl does not directly host the elements provided by the expanded DataTemplate.  
            //It uses a ContentPresenter to do the  work. 
            //If we want to use the FindName method we will have to pass that ContentPresenter as the second argument, instead of the ContentControl. 
            ContentPresenter myContentPresenter = GetVisualChild<ContentPresenter>(myListBoxItem);

            var grid = gridView.Columns[2].CellTemplate.FindName("txtListCartQuantity", myContentPresenter);
            string text = ((System.Windows.Controls.TextBox)(grid)).Text;

            int quantity;
            bool success = int.TryParse(text, out quantity);
            if (success)
                return quantity;
            return -1;
        }

        private T GetVisualChild<T>(Visual parent) where T : Visual
        {
            T child = default(T);

            for (int index = 0; index < VisualTreeHelper.GetChildrenCount(parent); index++)
            {
                Visual visualChild = (Visual)VisualTreeHelper.GetChild(parent, index);
                child = visualChild as T;

                if (child == null)
                    child = GetVisualChild<T>(visualChild);//Find Recursively

                else
                    return child;
            }
            return child;
        }



        private bool GetCreditDetails()
        {

            bool ans;
            bool successNum = false;
            bool successThree = false;
            int month;
            int year;
            bool monthSucc = int.TryParse(txtMonth.Text, out month);
            bool yearSucc = int.TryParse(txtYear.Text, out year);
            bool successDate = monthSucc && yearSucc;


            if (monthSucc && yearSucc)
            {
                if (int.Parse(txtMonth.Text) < 1 || int.Parse(txtMonth.Text) > 12)
                {
                    MessageBox.Show("Month is not valid");
                }
                if (int.Parse(txtYear.Text) < 2015)
                {
                    MessageBox.Show("Year is not valid");
                }
                else if (int.Parse(txtYear.Text) == 2015)
                {
                    if (int.Parse(txtMonth.Text) < DateTime.Now.Month)
                    {
                        MessageBox.Show("Card date has expired");
                    }
                }
            }
            else
            {
                MessageBox.Show("Invalid month or year");
            }
            long number;
            successNum = long.TryParse(txtCardNumber.Text, out number);
            if (successNum)
            {
                int cnt = 1;
                long n = long.Parse(txtCardNumber.Text);
                while (n / 10 > 0)
                {
                    n = n / 10;
                    cnt++;
                }
                if (cnt != 12)
                {
                    MessageBox.Show("Invalid Card Number");
                    successNum = false;
                }
                else
                {
                    successNum = true;
                }
            }
            int threeDig;
            successThree = int.TryParse(txtThreeDigits.Text, out threeDig);
            if (successThree)
            {
                int cnt2 = 1;
                int n2 = int.Parse(txtThreeDigits.Text);
                while (n2 / 10 > 0)
                {
                    n2 = n2 / 10;
                    cnt2++;
                }
                if (cnt2 != 3)
                {

                    MessageBox.Show("Invalid Card Three Digits");
                    successThree = false;
                }
                else
                {
                    successThree = true;
                }
            }

            return ans = successDate && successNum && successThree;
        }



        private void cmbPay_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PaymentMethod payment = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), (string)cmbPay.SelectedItem);
            if (_ibl.IsCreditCard(payment) && (_ibl.FindUserType(_user.UserName, _user.Password).Equals(User.userType.ClubMember)))
            {

                btnPayAndSave.IsEnabled = true;
            }
            else
            {
                btnPayAndSave.IsEnabled = false;
            }
        }

        private void btnViewSum_Click(object sender, RoutedEventArgs e)
        {
            if (ListCart.Items != null)
            {
                {
                    double price = GetTotalPrice();
                    MessageBox.Show("The Total Sum is " + price);
                }

            }
        }

        private void btnPay_Click(object sender, RoutedEventArgs e)
        {


            if (cmbPay.SelectedIndex != -1)
            {
                if (cmbPay.SelectedItem.Equals("Visa") || cmbPay.SelectedItem.Equals("MasterCard") || cmbPay.SelectedItem.Equals("AmericanExpress") || cmbPay.SelectedItem.Equals("Isracard"))
                {
                    if (GetCreditDetails())
                    {
                        if (_ibl.FindUserType(_user.UserName, _user.Password).Equals(User.userType.ClubMember))
                        {
                            PaymentMethod payment = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), (string)cmbPay.SelectedItem);
                            Dictionary<int, Transaction.SoldProducts> receipt = GetReciept();
                            if (receipt.Count > 0)
                            {
                                Transaction tran = _ibl.AddnewTransaction(DateTime.Now, false, payment, receipt);

                                ClubMember clubMember = _ibl.GetClubMemberByUser(_user.UserName);
                                _ibl.AddNewTransactionId(clubMember.memberID, tran.TransactionID);
                                MessageBox.Show("Your card's information saved successfully and you paid successfully!");
                            }
                            else
                            {
                                MessageBox.Show("Please choose products");
                            }
                        }
                        else
                        {
                            PaymentMethod payment = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), (string)cmbPay.SelectedItem);
                            Dictionary<int, Transaction.SoldProducts> receipt = GetReciept();
                            if (receipt.Count > 0)
                            {
                                Transaction tran = _ibl.AddnewTransaction(DateTime.Now, false, payment, receipt);

                                ClubMember clubMember = _ibl.GetClubMemberByUser(_user.UserName);
                                _ibl.AddNewTransactionId(clubMember.memberID, tran.TransactionID);
                                MessageBox.Show("Your card's information saved successfully and you paid successfully!");
                            }
                            else
                            {
                                MessageBox.Show("Please choose products");
                            }

                        }
                    }

                }
                else
                {
                    PaymentMethod payment = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), (string)cmbPay.SelectedItem);
                    Dictionary<int, Transaction.SoldProducts> receipt = GetReciept();
                    if (receipt.Count > 0)
                    {
                        Transaction tran = _ibl.AddnewTransaction(DateTime.Now, false, payment, receipt);

                        ClubMember clubMember = _ibl.GetClubMemberByUser(_user.UserName);
                        _ibl.AddNewTransactionId(clubMember.memberID, tran.TransactionID);
                        MessageBox.Show("Your card's information saved successfully and you paid successfully!");
                    }
                    else
                    {
                        MessageBox.Show("Please choose products");
                    }
                }
            }
            else
            {
                MessageBox.Show("Pleas choose payment method");
            }
        }

        private void btnAdminPurchase_Click(object sender, RoutedEventArgs e)
        {
            if (cmbPay.SelectedIndex != -1)
            {
                if (_ibl.FindUserType(_user.UserName, _user.Password).Equals(User.userType.Admin))
                {
                    if ((cmbPay.SelectedItem.Equals("Visa") || cmbPay.SelectedItem.Equals("MasterCard") || cmbPay.SelectedItem.Equals("AmericanExpress") || cmbPay.SelectedItem.Equals("Isracard")))
                    {

                        if (GetCreditDetails())
                        {
                            PaymentMethod payment = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), (string)cmbPay.SelectedItem);

                            Dictionary<int, Transaction.SoldProducts> receipt = GetReciept();
                            if (receipt.Count > 0)
                            {
                                Transaction tran = _ibl.AddnewTransaction(DateTime.Now, false, payment, receipt);
                                ClubMember clubMember = _ibl.GetClubMemberByUser(_user.UserName);
                                MessageBox.Show("Admin you have added transaction successfully!");
                            }
                            else
                            {
                                MessageBox.Show("Please choose products");
                            }

                        }
                        else
                        {
                            MessageBox.Show("Please try again");

                        }
                    }
                    else
                    {

                        PaymentMethod payment = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), (string)cmbPay.SelectedItem);
                        Dictionary<int, Transaction.SoldProducts> receipt = GetReciept();
                        _ibl.AddnewTransaction(DateTime.Now, false, payment, receipt);
                        if (receipt.Count > 0)
                        {
                            Transaction tran = _ibl.AddnewTransaction(DateTime.Now, false, payment, receipt);
                            ClubMember clubMember = _ibl.GetClubMemberByUser(_user.UserName);
                            MessageBox.Show("Admin you have added transaction successfully!");
                        }
                        else
                        {
                            MessageBox.Show("Please choose products");
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Pleas choose payment method");
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {

            if (_ibl.FindUserType(_user.UserName, _user.Password).Equals(User.userType.Admin))
            {
                adminMain adminMain = new adminMain(_ibl, _user);
                this.Hide();
                adminMain.Show();
            }
            if (_ibl.FindUserType(_user.UserName, _user.Password).Equals(User.userType.Manager))
            {
                ManagerMenu ManMain = new ManagerMenu(_ibl, _user);
                this.Hide();
                ManMain.Show();
            }
            if (_ibl.FindUserType(_user.UserName, _user.Password).Equals(User.userType.Worker))
            {
                WorkerMenu WorkMain = new WorkerMenu(_ibl, _user);
                this.Hide();
                WorkMain.Show();
            }
            if ((_ibl.FindUserType(_user.UserName, _user.Password).Equals(User.userType.Customer) || (_ibl.FindUserType(_user.UserName, _user.Password).Equals(User.userType.ClubMember))))
            {
                MainWindow P = new MainWindow();
                this.Hide();
                P.Show();
            }
        }

        private void txtMonth_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            txtMonth.Text = "";
        }

        private void txtYear_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            txtYear.Text = "";
        }

        private void ProgressBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }
    }

}


