﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_backend;


namespace GUI
{
    /// <summary>
    /// Interaction logic for SignUp.xaml
    /// </summary>
    public partial class SignUp : Window
    {

        IBL itsBL;

        public SignUp(IBL itsBl)
        {
            itsBL = itsBl;

            InitializeComponent();
            Show();
            FirstNameField.Visibility = Visibility.Hidden;
            LastNameField.Visibility = Visibility.Hidden;
            dpC.Visibility = Visibility.Hidden;
            IdField.Visibility = Visibility.Hidden;
            FirstName.Visibility = Visibility.Hidden;
            LastName.Visibility = Visibility.Hidden;
            DateOfBirth.Visibility = Visibility.Hidden;
            Gender.Visibility = Visibility.Hidden;
            Male.Visibility = Visibility.Hidden;
            Female.Visibility = Visibility.Hidden;
            Id.Visibility = Visibility.Hidden;
            DateOfBirth.Visibility = Visibility.Hidden;
        }




        private void clubMember_checked(object sender, RoutedEventArgs e)
        {
            FirstNameField.Visibility = Visibility.Visible;
            LastNameField.Visibility = Visibility.Visible;
            dpC.Visibility = Visibility.Visible;
            IdField.Visibility = Visibility.Visible;
            FirstName.Visibility = Visibility.Visible;
            LastName.Visibility = Visibility.Visible;
            dpC.Visibility = Visibility.Visible;
            Gender.Visibility = Visibility.Visible;
            Male.Visibility = Visibility.Visible;
            Female.Visibility = Visibility.Visible;
            Id.Visibility = Visibility.Visible;
            DateOfBirth.Visibility = Visibility.Visible;

        }



        private void customer_Checked(object sender, RoutedEventArgs e)
        {

            FirstNameField.Visibility = Visibility.Hidden;
            LastNameField.Visibility = Visibility.Hidden;
            dpC.Visibility = Visibility.Hidden;
            IdField.Visibility = Visibility.Hidden;
            FirstName.Visibility = Visibility.Hidden;
            LastName.Visibility = Visibility.Hidden;
            dpC.Visibility = Visibility.Hidden;
            Gender.Visibility = Visibility.Hidden;
            Male.Visibility = Visibility.Hidden;
            Female.Visibility = Visibility.Hidden;
            Id.Visibility = Visibility.Hidden;
            DateOfBirth.Visibility = Visibility.Hidden;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (customerClick.IsChecked.Value)
            {
                try
                {
                    User.userType type = User.userType.Customer;
                    string userName = UserNameField.Text;
                    int pass = Convert.ToInt32(PassField.Text);
                    User NewUser = new User(userName, pass, type);
                    itsBL.addUser(NewUser);
                    MessageBox.Show("Your Details Saved Successfly!");
                }
                catch (Exception)
                {
                    MessageBox.Show("Invalid Input");
                }
            }
            if (Club_Memberclick.IsChecked.Value)
            {
                try
                {
                    string userName = UserNameField.Text;
                    int pass = Convert.ToInt32(PassField.Text);
                    string firstName = FirstNameField.Text;
                    string lastName = LastNameField.Text;
                    int id = Convert.ToInt32(IdField.Text);

                    string IDd = IdField.Text;

                    if (IDd.Length != 9)
                    {
                        MessageBox.Show("Please enter a valid ID num (9 digits)");
                        return;
                    }
                    DateTime date = (DateTime)dpC.SelectedDate;

                    User.userType type = User.userType.ClubMember;
                    string gender = "";
                    if (Male.IsChecked == true)
                    {
                        gender = "Male";
                    }
                    else if (Female.IsChecked == true)
                    {
                        gender = "Female";
                    }

                    User NewUser = new User(userName, pass, type);
                    Random RndNum = new Random();
                    int memberid = RndNum.Next(100000000, 999999999);
                    itsBL.AddnewClubMember(memberid, date, id, firstName, lastName, gender, NewUser, date, 0, 0);


                    itsBL.addUser(NewUser);
                    MessageBox.Show("Your Details Saved Successfly!");
                }
                catch (Exception)
                {
                    MessageBox.Show("Invalid Input");
                }

            }




        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {

        }


    }
}