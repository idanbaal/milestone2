﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_backend;

namespace GUI
{
    /// <summary>
    /// Interaction logic for Transaction_Edit.xaml
    /// </summary>
    public partial class Transaction_Edit : Window
    {
        IBL itsBL;
        Transaction toEdit;
        string LtranId;
        Window last;
        public Transaction_Edit(Transaction tran,IBL bl, Window last)
        {
            this.last = last;
            itsBL = bl;
            toEdit = tran;
            LtranId=tran.TransactionID;
            InitializeComponent();

            tranID.Text = toEdit.TransactionID;
            Date.Text = "" + toEdit.DateTime;
            payM.Text = "" + toEdit.PaymentMethod;
            IsReturn.IsChecked = toEdit.IsAReturn;
            gridy_reciept.ItemsSource= toEdit.Receipt;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                toEdit.TransactionID = tranID.Text;
                toEdit.DateTime = Convert.ToDateTime(Date.Text);
                toEdit.IsAReturn = IsReturn.IsChecked.Value;
                toEdit.PaymentMethod = (PaymentMethod)Enum.Parse(typeof(PaymentMethod), payM.Text, true);

            }
            catch(Exception)
            {
                 MessageBox.Show("Invalid Input");
            }
            itsBL.updateTransaction(LtranId,toEdit.IsAReturn,toEdit.DateTime,toEdit.PaymentMethod,toEdit.TransactionID);
            MessageBox.Show("The Transaction was Updated Successfully");
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            last.Show();
            this.Close();
        }
    }
}
