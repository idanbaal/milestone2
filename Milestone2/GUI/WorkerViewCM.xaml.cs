﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_backend;

namespace GUI
{
    /// <summary>
    /// Interaction logic for WorkerViewCM.xaml
    /// </summary>
    public partial class WorkerViewCM : Window
    {
        private IBL itsBL;
        private User U;
        public WorkerViewCM(IBL IBL, User U)
        {
            this.itsBL = IBL;
            this.U = U;
            InitializeComponent();
            dgClubMember.ItemsSource = itsBL.GetAllClubMembers();
        }

        private void btnFilterCM_Click(object sender, RoutedEventArgs e)
        {
            List<ClubMember> C = new List<ClubMember>();
            try
            {
                int MemberID = Int32.Parse(memberID.ToString());
                C=itsBL.GetAllClubMemberByClubID(MemberID);
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid Input");
            }
            dgClubMember.ItemsSource = C;
        }

        private void btnFilterDateC_Click(object sender, RoutedEventArgs e)
        {
            List<ClubMember> C = new List<ClubMember>();
            try
            {
                DateTime date1 = (DateTime)dpC1.SelectedDate;
                DateTime date2 = (DateTime)dpC2.SelectedDate;
                C=itsBL.GetClubMemberListByDateOfBirth(date1, date2);
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid Input");
            }


            dgClubMember.ItemsSource = C;
        }

        private void btnGoBack_Click(object sender, RoutedEventArgs e)
        {
            WorkerMenu main = new WorkerMenu(itsBL, U);
            main.Show();
            this.Close();
        }

    }
}
