﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_backend;

namespace GUI
{
    /// <summary>
    /// Interaction logic for WorkerViewT.xaml
    /// </summary>
    public partial class WorkerViewT : Window
    {
         private IBL itsBL;
        private User U;
        public WorkerViewT(IBL IBL, User U)
        {
            this.itsBL = IBL;
            this.U = U;
            InitializeComponent();
            dgTransaction.ItemsSource = itsBL.GetAllTransactions();
        }


    private void btnFilterT_Click(object sender, RoutedEventArgs e)
    {
        List<Transaction> T = new List<Transaction>();
        try
        {
            DateTime date1 = (DateTime)dpT.SelectedDate;

            T=itsBL.GetAllTransactionListByDate(date1);
        }
        catch (Exception)
        {
            MessageBox.Show("Invalid Input");
        }


        dgTransaction.ItemsSource = T;
    }

    private void btnFilterPayment_Click(object sender, RoutedEventArgs e)
    {
        List<Transaction> T = new List<Transaction>();
        T=(itsBL.GetTransactionListByPaymentMethod((PaymentMethod)Enum.Parse(typeof(PaymentMethod), cbPayment.Text.ToString())));

        dgTransaction.ItemsSource = T;
    }

    private void btnGoBack_Click(object sender, RoutedEventArgs e)
    {
        WorkerMenu main = new WorkerMenu(itsBL, U);
        main.Show();
        this.Close();
    }




    }
}
