﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using DAL;
using BL;
using BL_backend;


namespace GUI
{
    
    public partial class adminMain : Window
    {

        private IBL itsBL;
        private User U;
        public adminMain(IBL IBL, User U)
        {
            this.itsBL = IBL;
            this.U = U;
            InitializeComponent();
        }

        private void btnAddEntities_click(object sender, RoutedEventArgs e)
        {
            AdminAdd add = new AdminAdd(itsBL, U);
            add.Show();
            this.Close();
        }


        private void btnDeleteEditData_Click(object sender, RoutedEventArgs e)
        {
            Edit_Entities edit = new Edit_Entities(itsBL,this);
            edit.Show();
            this.Hide();
        }

        private void btnQueries_Click(object sender, RoutedEventArgs e)
        {
            quries qu = new quries(itsBL, U);
            qu.Show();
            this.Close();
        }


        private void btnLogOut_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Close();
        }

        private void btnCart_Click(object sender, RoutedEventArgs e)
        {
            PurchaseWindow P = new PurchaseWindow(itsBL, U);
            P.Show();
            this.Close();
        }

       

    }
}
