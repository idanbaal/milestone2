﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Threading;

namespace GUI
{
    /// <summary>
    /// Interaction logic for load.xaml
    /// </summary>
    public partial class load : Window
    {
        private int count;
        private int percentage;
        public load(int count)
        {
            InitializeComponent();
            this.count = count;
            this.pbStatus.Maximum = count;
            percentage = 0;
        }
        private void Window_ContentRendered(object sender, EventArgs e)
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;

            worker.RunWorkerAsync();
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            for (int i = 0; i <= count; i++)
            {
                (sender as BackgroundWorker).ReportProgress(i);

                Thread.Sleep(70);

            }
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pbStatus.Value = e.ProgressPercentage;
            percentage = e.ProgressPercentage;
            this.Title = Convert.ToString((Math.Floor((double)(percentage) / count * 100))) + "%";
        }

        private void pbStatus_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

        }

        private void pbStatus_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }
    }
}