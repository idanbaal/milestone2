﻿#pragma checksum "..\..\Edit_Entities Manager.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "12291A64CE46C83D440E05A0C017E45C"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace GUI {
    
    
    /// <summary>
    /// Edit_Entities_Manager
    /// </summary>
    public partial class Edit_Entities_Manager : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\Edit_Entities Manager.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabControl tabControl;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\Edit_Entities Manager.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid gridy;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\Edit_Entities Manager.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid gridy_product;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\Edit_Entities Manager.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid gridy_employee;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\Edit_Entities Manager.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid gridyDp;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\Edit_Entities Manager.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid gridyTran;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\Edit_Entities Manager.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ProgressBar progBar;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/GUI;component/edit_entities%20manager.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Edit_Entities Manager.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.tabControl = ((System.Windows.Controls.TabControl)(target));
            
            #line 10 "..\..\Edit_Entities Manager.xaml"
            this.tabControl.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.TabControl_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.gridy = ((System.Windows.Controls.DataGrid)(target));
            
            #line 18 "..\..\Edit_Entities Manager.xaml"
            this.gridy.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.gridy_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            
            #line 19 "..\..\Edit_Entities Manager.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            
            #line 20 "..\..\Edit_Entities Manager.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click_1);
            
            #line default
            #line hidden
            return;
            case 5:
            this.gridy_product = ((System.Windows.Controls.DataGrid)(target));
            
            #line 26 "..\..\Edit_Entities Manager.xaml"
            this.gridy_product.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.gridy_product_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            
            #line 29 "..\..\Edit_Entities Manager.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click_2);
            
            #line default
            #line hidden
            return;
            case 7:
            
            #line 30 "..\..\Edit_Entities Manager.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click_3);
            
            #line default
            #line hidden
            return;
            case 8:
            this.gridy_employee = ((System.Windows.Controls.DataGrid)(target));
            
            #line 35 "..\..\Edit_Entities Manager.xaml"
            this.gridy_employee.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.gridy_employe_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 9:
            
            #line 38 "..\..\Edit_Entities Manager.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.editEmpoyee);
            
            #line default
            #line hidden
            return;
            case 10:
            
            #line 39 "..\..\Edit_Entities Manager.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.removeEmployee);
            
            #line default
            #line hidden
            return;
            case 11:
            this.gridyDp = ((System.Windows.Controls.DataGrid)(target));
            
            #line 44 "..\..\Edit_Entities Manager.xaml"
            this.gridyDp.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.gridtDp_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 12:
            
            #line 47 "..\..\Edit_Entities Manager.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.editdp);
            
            #line default
            #line hidden
            return;
            case 13:
            
            #line 48 "..\..\Edit_Entities Manager.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.removedp);
            
            #line default
            #line hidden
            return;
            case 14:
            this.gridyTran = ((System.Windows.Controls.DataGrid)(target));
            
            #line 53 "..\..\Edit_Entities Manager.xaml"
            this.gridyTran.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.gridtTran_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 15:
            
            #line 56 "..\..\Edit_Entities Manager.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.editTran);
            
            #line default
            #line hidden
            return;
            case 16:
            
            #line 57 "..\..\Edit_Entities Manager.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.removeTran);
            
            #line default
            #line hidden
            return;
            case 17:
            
            #line 61 "..\..\Edit_Entities Manager.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click_4);
            
            #line default
            #line hidden
            return;
            case 18:
            this.progBar = ((System.Windows.Controls.ProgressBar)(target));
            
            #line 62 "..\..\Edit_Entities Manager.xaml"
            this.progBar.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.ProgressBar_ValueChanged);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

