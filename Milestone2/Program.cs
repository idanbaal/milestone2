﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PL;
using BL;
using DAL;
using LINQ_DAL;

namespace MainProg
{
    class Program
    {
        static void Main(string[] args)
        {
            //Recusivly build the 3 layers
            
            IDAL aDAL = new LINQ_DAL();
            IBL aBL = new product_BL(aDAL);
            IPL aPL = new PL_CLI(aBL);


            //Start the presentation layer
            aPL.Run();
            

            
        }
    }
}
