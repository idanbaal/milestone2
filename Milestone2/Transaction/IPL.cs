﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_backend;
using BL;

namespace PL
{
    public interface IPL
    {
        // Starts the Presentaton Layer
        void Run();
    }
}
