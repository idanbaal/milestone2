﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BL;
using BL_backend;
using DAL;
using NUnit;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        static IDAL idal = new LINQ_DAL();
        product_BL blTest = new product_BL(idal);

        [TestMethod]
        public void AddProduct()
        {

            blTest.AddnewProduct("HDMI Cable", BL_backend.productTypes.Electronics, 121212121, 141414141, true, 50, 150);
            Assert.AreEqual("HDMI Cable", idal.FindProductByID(121212121).getName());
            blTest.deleteProduct(121212121);
            
        }

        [TestMethod]
        public void EditProduct()
        {
            blTest.AddnewProduct("HDMI Cable", BL_backend.productTypes.Electronics, 121212121, 141414141, true, 50, 150);
            blTest.updateProduct( 121212121,150,BL_backend.productTypes.Electronics,141414141,150,121212121,true); //edit the stock number of the product
            Assert.AreEqual(150, idal.FindProductByID(121212121).StockCount);
            blTest.deleteProduct(121212121);

        }

        [TestMethod]
        public void RemoveProduct()
        {
            blTest.AddnewProduct("HDMI Cable", BL_backend.productTypes.Electronics, 121212121, 141414141, true, 50, 150);
            blTest.deleteProduct(121212121);
            Assert.AreEqual(null, idal.FindProductByID(121212121));  //the product doesn't exist so find product return t

        }

        /// <summary>
        /// *******New 7 NUnits****
        /// </summary>


        ///***Users
        [TestMethod]
        public void AddUser()
        { 
              blTest.addUser(new User("Israel",123456,User.userType.Admin));
            Assert.AreEqual(true, blTest.FindUser("Israel",123456));
            blTest.removeUser(new User("Israel", 123456, User.userType.Admin));
        }

        
    

        [TestMethod]
        public void RemoveUser()
        {
            blTest.addUser(new User("Israel", 123456, User.userType.Admin));
            blTest.removeUser(new User("Israel", 123456, User.userType.Admin));
            Assert.AreEqual(false, blTest.FindUser("Israel", 123456));
        }



        /// <summary>
        /// ***Departments
        /// </summary>
        [TestMethod]
        public void AddDipartment()
        {
            blTest.AddNewDepartment(123456, "Electronics");
          
            Assert.AreEqual(true, blTest.FindDepartmentByID(123456));

            blTest.deleteDepartment(123456);
        }

        [TestMethod]
        public void RemoveDepartment()
        {
            blTest.AddNewDepartment(123456, "Electronics");
            blTest.deleteDepartment(123456);

            Assert.AreEqual(false, blTest.FindDepartmentByID(123456));

          
        }

        //**Employee
        [TestMethod]
        public void AddEmployee()
        {
           
            blTest.AddNewEmployee(2500, 123456789, 987654321,301456987, "Ben", "Koglar", "Male");
            Assert.AreEqual(true, blTest.FindEmployeeByID(301456987));
            Assert.AreEqual("Ben", idal.FindEmployeeByID(301456987).getFirstName());
            blTest.deleteEmployee(301456987);
        }


 

        [TestMethod]
        public void RemoveEmployee()
        {
            blTest.AddNewEmployee(2500, 123456789, 301456987, 987654321, "Ben", "Koglar", "Male");
            blTest.deleteEmployee(301456987);
            Assert.AreEqual(false, blTest.FindEmployeeByID(301456987));
        
        }


        [TestMethod]
        public void AddClubMembet()
        {
            blTest.AddnewClubMember(12345, new DateTime(1988, 05, 20), 301555444, "Tal", "Boz", "Female", new User("Tal", 1234), new DateTime(2019, 01, 01), 123456789111, 123);
            Assert.AreEqual(true, blTest.FindClubMemberByID(301555444));
            blTest.deleteClubMember(301555444);

        }






    }
}
