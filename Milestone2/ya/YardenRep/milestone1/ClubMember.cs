﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace ConsoleApplication1
{
    class ClubMember : Person
    {
        private List<int> transactionID=new List<int>();
        private int memberID;
        private DateTime dateOfBirth;


        public ClubMember(int memberID, DateTime dateOfBirth, int teudatZeut, string firstName, string lastName, string gender) : base(teudatZeut, firstName, lastName, gender)
        {
            this.memberID = memberID;
            this.dateOfBirth = dateOfBirth;

        }

        
        public int MemberID
        {
            get { return memberID; }
            set { memberID = value; }
        }

        public DateTime DateOfBirth
        {
            get { return dateOfBirth; }
            set { dateOfBirth = value; }
        }
        public List<int> TransactionID
        {
            get { return transactionID; }
            set { transactionID = value; }
        }


    }

}