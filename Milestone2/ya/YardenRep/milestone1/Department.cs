﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Department
    {
        private string name;


        private int departmentID;

              
        public Department(string name, int departmentID)
        {
            this.name = name;
            this.departmentID = departmentID;
                    }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public int DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }


    }
}
