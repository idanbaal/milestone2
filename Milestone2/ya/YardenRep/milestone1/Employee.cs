﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Employee : Person
    {
        private int departmentID, supervisor;
        private double salary;


        public Employee (int salary, int departmentID, int supervisor, int teudatZeut, string firstName, string lastName, string gender): base(teudatZeut,firstName,lastName,gender) 
        {
            this.supervisor = supervisor;
            this.salary = salary;
            this.departmentID = departmentID;
        }



public int Supervisor
{
  get { return supervisor; }
  set { supervisor = value; }
}

public int DepartmentID
{
  get { return departmentID; }
  set { departmentID = value; }
}
        

public double Salary
        {
            get { return salary; }
            set { salary = value; }
        }
    }
}