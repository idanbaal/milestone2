﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Product
    {
        private string name;

        private Enum type;


        private int inventoryID;

        private int location;

        private Boolean inStock;

        private int stockCount;


        private double price;

        
        
        public Product(string name, Enum type, int inventoryID, int location, Boolean inStock, int stockCount, double price)
        {
            this.name = name;
            this.type = type;
            this.inventoryID = inventoryID;
            this.location = location;
            this.inStock = inStock;
            this.stockCount = stockCount;
            this.price = price;
        }



        public string Name
        {
            get { return name; }
            set { name = value; }
        }



        public Enum Type
        {
            get { return type; }
            set { type = value; }
        }


        public int InventoryID
        {
            get { return inventoryID; }
            set { inventoryID = value; }
        }

        public int Location
        {
            get { return location; }
            set { location = value; }
        }

        public Boolean InStock
        {
            get { return inStock; }
            set { inStock = value; }
        }
        public int StockCount
        {
            get { return stockCount; }
            set { stockCount = value; }
        }

        public double Price
    {
            get { return price; }
            set { price = value; }
        }

    }
}
