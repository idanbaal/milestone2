﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace ConsoleApplication1
{
    enum PaymentMethod
    {
        Cash,
        Visa,
        MasterCard,
        AmericanExpress,
        Isracard,
        Check,
        credit,
        Other
    }
    class Transaction
    {
        private int transactionID;
        private DateTime dateTime;
        private Boolean isAReturn;
        private List<GenericUriParser> receipt= new List<GenericUriParser>();
        private PaymentMethod paymentMethod;

        
        public Transaction(int transactionID, DateTime dateTime, Boolean isAReturn, PaymentMethod paymentMethod)
        {
            this.transactionID = transactionID;
            this.dateTime = dateTime;
            this.isAReturn = isAReturn;
            this.paymentMethod = paymentMethod;
        }


        public DateTime DateTime
        {
            get { return dateTime; }
            set { dateTime = value; }
        }


        public Boolean IsAReturn
        {
            get { return isAReturn; }
            set { isAReturn = value; }
        }
        public PaymentMethod PaymentMethod
        {
            get { return paymentMethod; }
            set { paymentMethod = value; }
        }

    }
}
